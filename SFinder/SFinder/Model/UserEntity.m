//
//  UserEntity.m
//  SFinder
//
//  Created by Steffen Gruenewald on 19/10/2016.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "UserEntity.h"
#import "DBManager.h"
#import "ReqConst.h"
#import "CommonUtils.h"

@implementation UserEntity

@synthesize _idx, _age, _sex, _name, _phone, _photoUrl, _aboutMe, _distance, _userState, _latitude, _longitude, _address, _email;
@synthesize _roomList, _userList, _friendList, _friendCount;
@synthesize notReadCount, _isLoggedIn;
@synthesize _anal, _bdsm, _devot, _figure, _height, _kisses, _classic,_leather,_spanish,_bisexual, _blow_job, _dominant, _hand_job, _hair_color, _deep_throat, _dirty_talks, _stripetease, _transsexuel, _delicate_sex, _masturbation, _without_tabu, _rolling_games, _escort_service;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _idx = 0;
        _age = 0;
        _sex = -1;
        _name = @"";
        
        _email = @"";
        _phone = @"";
        _aboutMe = @"";
        _address = @"";
        _distance = 0.0;
        _userState = 0;
        _latitude = 0.0;
        _longitude = 0.0;
        _friendCount = 0;
        _isLoggedIn = NO;
        
        _height = 100.0;
        _figure = @"";
        _age = 4;
        _hair_color = @"";
        _anal = 0;
        _escort_service = 0;
        _rolling_games = 0;
        _bdsm = 0;
        _bisexual = 0;
        _blow_job = 0;
        _delicate_sex = 0;
        _deep_throat = 0;
        _devot = 0;
        _dirty_talks = 0;
        _dominant = 0;
        _escort_service = 0;
        _hand_job = 0;
        _classic = 0;
        _kisses = 0;
        _leather = 0;
        _masturbation = 0;
        _spanish = 0;
        _stripetease = 0;
        _without_tabu = 0;
        _transsexuel = 0;
        
        
        _photoUrl = [[NSMutableArray alloc] init];
        _friendList = [[NSMutableArray alloc] init];
        _userList = [[NSMutableArray alloc] init];
        _roomList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (BOOL) isValid {
    
    if(_idx > 0)
        return YES;
    
    return NO;
}

- (void) addFriend:(UserEntity *)_other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            return;
        }
    }
    
    _friendCount ++;
    [_friendList addObject:_other];
}

- (void) deleteFriend:(UserEntity *) _other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            _friendCount --;
            [_friendList removeObject:_existFriend];
            return;
        }
    }
}

- (BOOL) isEqual:(UserEntity *)object {
    
    if(_idx == object._idx) {
        return YES;
    }
    
    return NO;
}

- (BOOL) isExistFriend:(UserEntity *)_other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            return YES;
        }
    }
    
    return NO;
}

- (UserEntity *) getFriend:(int) idx {
    
    for(UserEntity * friend in _friendList) {
        
        if (friend._idx == idx) {
            
            return friend;
        }
    }
    
    return nil;
}

//- (BOOL) isBlockedUser: (int) _senderIdx {
//    
//    for (int i = 0; i < _blockList.count; i ++) {
//        
//        if (_senderIdx == [_blockList[i] intValue]) {
//            
//            return YES;
//        }
//    }
//    
//    return NO;
//}

- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket {
    
    for(RoomEntity * _entity in _roomList) {
        
        // upadte chat room information with none read message
        // in case of inviting -  update participants, participants name, room displayname
        if([_revPacket._roomName isEqualToString:_entity._name]) {
            
            if(_revPacket._chatType == _TEXT) {
                _entity._recentContent = _revPacket._bodyString;
            } else {
                _entity._recentContent = NSLocalizedString(@"Sent_File", nil);
            }
            
            _entity._recentDate = _revPacket._sentTime;
            _entity._recentCounter ++;
            
            // in case that room name is same but the participants are changed - inviting
            if(![_entity._participantsName isEqualToString:_revPacket._participantName]) {
                
                _entity._participantsName = _revPacket._participantName;
                [_entity updateParticipantWithPartName:_revPacket._participantName withBlock:nil];
            }
            
            // update db
            [[DBManager getSharedInstance] updateRoomWithName:_entity._name participant_name:_entity._participantsName recentMessage:_entity._recentContent recentTime:_entity._recentDate recetCounter:_entity._recentCounter];
            
            notReadCount ++;
            //            [APPDELEGATE notifyReceiveNewMessage];
            
            return YES;
        }
    }
    
    return NO;
}

// update chat room information with received message
// if user has not current room, then add it into users's room list
- (void) updateUserRoomList:(XmppPacket *) _revPacket {
    
    [CommonUtils vibrate];
    
    // check user'r room list, then if user has the current room, update it with received message
    if(![self updatedExistRoom:_revPacket]) {
        
        // get other user's idx for get his information
        NSArray *ids = [_revPacket._participantName componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx != _idx) {
                
                other = idx;
                break;
            }
        }
        
        // get user's info and then add new chat room into user's room ist
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETUSERINFO, other];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                
//                _entity._firstName = [_dictParticipant valueForKey:RES_FIRSTNAME];
//                _entity._lastName = [_dictParticipant valueForKey:RES_LASTNAME];
//                
//                _entity._major = [[_dictParticipant valueForKey:RES_MAJOR] intValue];
//                _entity._year = [[_dictParticipant valueForKey:RES_YEAR] intValue];
//                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
//                _entity._status = [[_dictParticipant valueForKey:RES_STATUS] intValue];
//                _entity._interest = [[_dictParticipant valueForKey:RES_INTEREST] intValue];
//                
//                _entity._favMovie = [_dictParticipant valueForKey:RES_MOVIE];
//                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
//                _entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTOURL];
//                
//                _entity._friendCount = [[_dictParticipant valueForKey:RES_FRIENDCOUNT] intValue];
                
                [_arrParticipants addObject:_entity];
                
                // create new chat room
                
                RoomEntity * _newRoom = [[RoomEntity alloc] initWithName:_revPacket._roomName participants:_arrParticipants];
                
                // update with new info
                _newRoom._recentContent = _revPacket._bodyString;
                
                _newRoom._recentDate = _revPacket._sentTime;
                _newRoom._recentCounter = 1;
                
                [self addRoomList:_newRoom];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) addRoomList:(RoomEntity *) _newRoom {
    
    // increase none read message count as user has received new message
    // before adding a room, check user's room list again, because of ansynchronize commnunication
    notReadCount ++;
    //    [APPDELEGATE notifyReceiveNewMessage];
    
    for(RoomEntity * _existRoom in _roomList) {
        
        // already exist room
        if([_existRoom._name isEqualToString:_newRoom._name]) {
            _existRoom._recentContent = _newRoom._recentContent;
            _existRoom._recentDate = _newRoom._recentDate;
            _existRoom._recentCounter ++;
            
            // get room display name with participants.
            if(![_existRoom._participantsName isEqualToString:_newRoom._participantsName]) {
                [_existRoom makeRoomDisplayName];
            }
            
            // add a room into local database
            // it is done by with only one functions - adding & updating
            [[DBManager getSharedInstance] saveRoomWithName:_newRoom._name participant_name:_newRoom._participantsName recent_message:_newRoom._recentContent recent_time:_newRoom._recentDate recent_counter:_newRoom._recentCounter];
            
            return;
        }
    }
    
    [_roomList addObject:_newRoom];
    
    // add a room into local database
    [[DBManager getSharedInstance] saveRoomWithName:_newRoom._name participant_name:_newRoom._participantsName recent_message:_newRoom._recentContent recent_time:_newRoom._recentDate recent_counter:_newRoom._recentCounter];
}

// user profile image setting
- (NSString *) getProfile {
    
    if (_photoUrl.count != 0) {
        return _photoUrl[0];
    } else {
        return @"";
    }
}

@end

//
//  UserEntity.h
//  SFinder
//
//  Created by Steffen Gruenewald on 19/10/2016.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RoomEntity.h"
#import "XmppPacket.h"

@interface UserEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString *_email;
@property (nonatomic, strong) NSString *_name;
@property (nonatomic, strong) NSString *_phone;
@property (nonatomic, strong) NSString *_aboutMe;
@property (nonatomic, strong) NSString *_address;
@property (nonatomic, strong) NSMutableArray *_photoUrl;
@property (nonatomic) float _latitude;
@property (nonatomic) float _longitude;
@property (nonatomic) float _distance;
@property (nonatomic) int _sex;
@property (nonatomic) int _age;
@property (nonatomic) int _userState;
@property (nonatomic) int _friendCount;
@property (nonatomic) BOOL _isLoggedIn;

//  for filter

@property (nonatomic)float _height;
@property (nonatomic, strong) NSString * _figure;
@property (nonatomic, strong) NSString * _hair_color;
@property (nonatomic) int _anal;
@property (nonatomic) int _bdsm;
@property (nonatomic) int _bisexual;
@property (nonatomic) int _blow_job;
@property (nonatomic) int _delicate_sex;
@property (nonatomic) int _deep_throat;
@property (nonatomic) int _devot;
@property (nonatomic) int _dirty_talks;
@property (nonatomic) int _dominant;
@property (nonatomic) int _escort_service;
@property (nonatomic) int _hand_job;
@property (nonatomic) int _classic;
@property (nonatomic) int _kisses;
@property (nonatomic) int _leather;
@property (nonatomic) int _rolling_games;
@property (nonatomic) int _masturbation;
@property (nonatomic) int _spanish;
@property (nonatomic) int _stripetease;
@property (nonatomic) int _without_tabu;
@property (nonatomic) int _transsexuel;

//---------

@property (nonatomic, strong) NSMutableArray *_userList;
@property (nonatomic, strong) NSMutableArray *_roomList;
@property (nonatomic, strong) NSMutableArray *_friendList;

// not read message count
@property (nonatomic) int notReadCount;

// check the user validation with id
- (BOOL) isValid;

// add or delete friend
- (void) addFriend:(UserEntity *) _other;
- (void) deleteFriend:(UserEntity *) _other;

- (BOOL) isEqual:(UserEntity *)object;

// check if a user is friend with param _user
- (BOOL) isExistFriend:(UserEntity *)_other;

// get friend with idx
- (UserEntity *) getFriend:(int) idx;

// update room information with recieved packet
- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket;

// update user's chat room list with received packet
- (void) updateUserRoomList:(XmppPacket *) _revPacket;

// add new room into user's chat room list
- (void) addRoomList:(RoomEntity *) _newRoom;

// return true if user is a blocked user, otherwise return false
//- (BOOL) isBlockedUser: (int) _senderIdx;

// user profile image setting
- (NSString *) getProfile;

@end

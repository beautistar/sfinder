//
//  RoomEntity.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserEntity;

@interface RoomEntity : NSObject {
    
    NSString *_name;                            // room name        1_2_3     : initial created name
    NSString *_participantsName;                // room participant ID  1_2_3_4_5 : participant id array
    NSString *_displayName;                     // make display name with participants name
    
    NSString *_recentContent;                   // last received message
    NSString *_recentDate;                      // last received date & time
    int _recentCounter;                         // none read message count
    
    int _currentUsers;                          // current users
    
    BOOL _isSelected;                           // it presents room selection state - true
    
    NSMutableArray * _participants;             // FriendEntity array
    NSMutableArray * _chatList;                 // ChatEntity array

}


@property (nonatomic, retain) NSString *_name;
@property (nonatomic, retain) NSString * _participantsName;
@property (nonatomic, retain) NSString  *_displayName;

@property (nonatomic, retain) NSString * _recentContent;
@property (nonatomic, retain) NSString * _recentDate;

@property (nonatomic) int _recentCounter;
@property (nonatomic) int _currentUsers;

@property (nonatomic, retain) NSMutableArray *_participants;
@property (nonatomic, retain) NSMutableArray *_chatList;

@property (nonatomic) BOOL _isSelected;

- (instancetype) initWithParticipants:(NSMutableArray *) participants;
- (instancetype) initWithName:(NSString *) roomName participants:(NSMutableArray *)participants;

- (void) updateParticipantWithPartName:(NSString *) participantsName withBlock:(void(^)(void)) updatedRoomDisplayName;
- (void) updateParticiapntWithParticipants: (NSMutableArray *) newParticipants;


- (BOOL) equals:(id) other;

- (UserEntity *) getParticipant : (int) idx;

- (NSString *) makeRoomDisplayName;

- (int) getCurrentUsers;

@end

//
//  RoomEntity.m
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "RoomEntity.h"
#import "CommonUtils.h"


@implementation RoomEntity

@synthesize _name, _displayName, _participantsName, _recentContent, _recentDate;

@synthesize _recentCounter, _currentUsers;

@synthesize _participants, _chatList;

@synthesize _isSelected;

- (instancetype) init
{
    if(self = [super init])
    {
        // initialize code here
        
        _name = @"";
        _participantsName = @"";
        _displayName = @"";
        _recentContent = @"";
        _recentDate = @"";
        
        _recentCounter = 0;
        _currentUsers = 0;
        
        _isSelected = NO;
        
        _participants = [NSMutableArray array];
        _chatList = [NSMutableArray array];
    }
    
    return self;
}

// make a room as selection friend
- (instancetype) initWithParticipants:(NSMutableArray *) participants {
    
    if(self = [self init]) {
        _participants = participants;
        _name = [self makeRoomName];
        _participantsName = [self makeParticipantsName];
        
        _displayName = [self makeRoomDisplayName];
    }
    
    return self;
}

- (instancetype) initWithName:(NSString *) roomName participants:(NSMutableArray *)participants {
    
    
    if(self = [self init]) {
        
        _name = roomName;
        _participants = participants;
        _participantsName = [self makeParticipantsName];
        
        _displayName = [self makeRoomDisplayName];
    }
    
    return self;
}

- (NSString *) makeRoomName {
    
    NSString * roomName = @"";
    
    NSMutableArray * _nameArray = [NSMutableArray array];
    
    for(UserEntity * _user in _participants) {
        [_nameArray addObject:[NSNumber numberWithInt:_user._idx]];
    }
    
    [_nameArray addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
    
    NSArray * _sortedArray=  [_nameArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    for(int i = 0; i < [_sortedArray count]; i ++) {
        roomName = [NSString stringWithFormat:@"%@_%i", roomName, [(NSNumber *)_sortedArray[i] intValue]];
    }
    
    roomName = [roomName substringFromIndex:1];
    
    return roomName;
}

// participant name include user's id
// participant array doesn't include users id.
- (NSString * ) makeParticipantsName {
    
    NSString  * _partsName = @"";
    
    NSMutableArray * _nameArray = [NSMutableArray array];
    
    for(UserEntity * _user in _participants) {
        [_nameArray addObject:[NSNumber numberWithInt:_user._idx]];
    }
    
    [_nameArray addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
    
    NSArray * _sortedArray=  [_nameArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    for(int i = 0; i < [_sortedArray count]; i ++) {
        _partsName = [NSString stringWithFormat:@"%@_%i", _partsName, [(NSNumber *)_sortedArray[i] intValue]];
    }
    
    _partsName = [_partsName substringFromIndex:1];
    
    return _partsName;
}

- (NSString *) makeRoomDisplayName {
    
    NSString * roomDisplayName = @"";
    
    NSArray *_sortedArray = [_participants sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSNumber * first = [NSNumber numberWithInt:((UserEntity*)obj1)._idx];
        NSNumber * second = [NSNumber numberWithInt:((UserEntity*)obj2)._idx];
        
        return [first compare:second];
    }];
    
    for(UserEntity * _user in _sortedArray) {
        roomDisplayName = [NSString stringWithFormat:@"%@, %@", roomDisplayName, _user._name];
    }
    
    roomDisplayName = [roomDisplayName substringFromIndex:1];
    
    return roomDisplayName;
}

- (BOOL) equals:(id) other {
    
    RoomEntity *_other = (RoomEntity *) other;
    return ([_other._name isEqualToString:_name]);
}

// idx - chatting friend idx
// check if there is a friend with idx in participants...

- (UserEntity *) getParticipant:(int) idx {
    
    for(UserEntity * _user in _participants) {
        
        if (_user._idx == idx) {
            
            return _user;
        }
    }
    
    return nil;
}

/**
 **     in case of received invite message, when to invite other user at the room
 **     participantsName : 1_2_3...... update with received name...
 **     change self._participantsName......with new name
 **/
- (void) updateParticipantWithPartName:(NSString *) participantsName withBlock:(void (^)(void)) updatedRoomDisplayName{
    
    
        //        UserEntity * _user = APPDELEGATE.Me;
        //    
        //        // make server url
        //        NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GET_ROOM_INFO, participantsName];
        //    
        //        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        //        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //    
        //            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        //    
        //            if(nResult_Code == CODE_SUCCESS) {
        //    
        //                NSMutableArray * _roomParticipants = [responseObject objectForKey:RES_USER_LIST];
        //    
        //                for (int i = 0; i < [_roomParticipants count]; i ++) {
        //    
        //                    NSDictionary * _dict = _roomParticipants[i];
        //    
        //                    int _roomParticipantIdx = [[_dict valueForKey:RES_IDD] intValue];
        //    
        //                    if(_roomParticipantIdx == APPDELEGATE.Me._idx) continue;
        //    
        //                    FriendEntity * roomParticipant = [self getParticipant:_roomParticipantIdx];
        //    
        //                    if(roomParticipant == nil) {
        //    
        //                        if([_user getFriend:_roomParticipantIdx] != nil) {
        //    
        //                            [_participants addObject:[_user getFriend:_roomParticipantIdx]];
        //    
        //                        } else {
        //    
        //                            FriendEntity * roomParticipant = [[FriendEntity alloc] init];
        //                            roomParticipant._idx = [[_dict valueForKey:RES_IDD] intValue];
        //                            roomParticipant._name = [_dict valueForKey:RES_NAME];
        //                            roomParticipant._email = [_dict valueForKey:RES_EMAIL];
        //                            roomParticipant._phoneNumber = [_dict valueForKey:RES_PHONE_NUMBER];
        //                            roomParticipant._photoUrl = [_dict valueForKey:RES_PHOTO_URL];
        //                            roomParticipant._bgUrl = [_dict valueForKey:RES_BG_URL];
        //                            roomParticipant._label = [_dict valueForKey:RES_LABEL];
        //    
        //                            [_participants addObject:roomParticipant];
        //                        }
        //                    }
        //                }
        //    
        //                _displayName = [self makeRoomDisplayName];
        //    
        //                if(updatedRoomDisplayName)
        //                    updatedRoomDisplayName();
        //            }
        //    
        //        } failure:^(NSURLSessionTask *operation, NSError *error) {
        //    
        //            NSLog(@"Error: %@", error);
        //    
        //            [self updateParticipantWithPartName:participantsName withBlock:updatedRoomDisplayName];
        //        }];
}

/**
 **     inviting other user
 **     update participant with param... participants
 **     ... to invite friends...
 **/

- (void) updateParticiapntWithParticipants: (NSMutableArray *) newParticipants {
    
    [_participants addObjectsFromArray:newParticipants];
    
    // update _participant name and _displayName
    _participantsName = [self makeParticipantsName];
    
    [[DBManager getSharedInstance] updateRoomWithName:_name participant_name:_participantsName];
    _displayName = [self makeRoomDisplayName];
}

- (int) getCurrentUsers {
    return (int)[_participants count];
}



@end

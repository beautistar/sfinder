//
//  XmppEndPoint.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "DCMessageDelegate.h"

#import "RoomEntity.h"
#import "XmppPacket.h"


@interface XmppEndPoint : NSObject <XMPPStreamDelegate, XMPPRoomDelegate> {
    
    NSString * hostName;
    int hostPort;
    NSString * conferenceService;
    
    XMPPStream * xmppStream;
    XMPPReconnect * xmppReconnect;
    
    XMPPRoom * xmppJoinRoom;                        // chatting room when user chat with his or friend, entering room
    XMPPRoomMemoryStorage * xmppJoinRoomStorage;
    
    XMPPJID * xmppRoomJIDPaused;                    // room id when the app is entering in a bakcground 앱이 배경으로 들어갈때 당시의 방JID
    
    int userId;                                     // xmpp user id & password
    NSString * password;
    
    BOOL isXmppConnected;
    BOOL customCertEvaluation;
    
    __unsafe_unretained NSObject <DCMessageDelegate> * _messageDelegate;                    // message receive delegate outside of room
    __unsafe_unretained NSObject <DCRoomMessageDelegate> * _roomMesssageDelegate;           // message receive delegate inside of room
    __unsafe_unretained NSObject <XmppCustomReconnectionDelegate> *_reconnectionDelegate;   // xmpp reconnection delegate
    __unsafe_unretained NSObject <XmppFriendRequestDelegate> *_friendRequestDelegate;
    
    BOOL isSentFriendRequest;
}

@property (nonatomic) BOOL isXmppConnected;
@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;

@property (nonatomic, strong, readonly) XMPPRoom * xmppJoinRoom;
@property (nonatomic, strong, readonly) XMPPRoomMemoryStorage * xmppJoinRoomStorage;

@property (nonatomic, retain) XMPPJID * xmppRoomJIDPaused;

@property (nonatomic, assign) id _messageDelegate;
@property (nonatomic, assign) id _roomMessageDelegate;
@property (nonatomic, assign) id _reconnectionDelegte;
@property (nonatomic, assign) id _friendRequestDelegate;

@property (nonatomic) BOOL isSentFriendRequest;

- (instancetype) initWithHostName:(NSString *) p_strHostName hostPort:(int) p_nHostPort;

//- (BOOL) connect;
- (void) setupStream;
- (void) teardownStream;
- (BOOL) connect:(int) p_nUserId password:(NSString *)p_strPwd;
- (void) disconnect;

- (void) sendPacket:(XmppPacket *)_sendPacket;
- (void) sendPacket:(XmppPacket *)_sendPacket to:(int)friendIdx;

- (void) EnterChattingRoom :(RoomEntity *) _room;
- (void) enterChattingRoomForFriendRequest: (RoomEntity *) requestRoom;
-(void) leaveRoomWithJID:(RoomEntity *)_room;

- (void) EnterRoomInBg:(XMPPJID *) roomJID;
- (void) leaveRoomInBg;

- (void) registerDeviceToken:(NSString *) deviceToken;


@end

//
//  ChatListCell.m
//  SFinder
//
//  Created by AOC on 28/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ChatListCell.h"


@implementation ChatListCell

@synthesize imvProfile, lblName, lblLastMsg, lblTime;

- (void) awakeFromNib {
    
    //initialize code
}

- (void) setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}

- (void) setUserModel:(UserEntity *) _entity {
    
    [imvProfile setImageWithURL:[NSURL URLWithString:_entity.getProfile] placeholderImage:[UIImage imageNamed:@"profile"]];
    lblName.text = _entity._name;
    lblLastMsg.text = _entity._aboutMe;
    
}

- (void) setRoomModel:(RoomEntity *) _roomEntity {
    
    UserEntity *participant = _roomEntity._participants[0];
    [imvProfile setImageWithURL:[NSURL URLWithString:participant.getProfile] placeholderImage:[UIImage imageNamed:@"profile"]];
    lblName.text = _roomEntity._displayName;
    lblLastMsg.text = _roomEntity._recentContent;
    
    lblTime.text = _roomEntity._recentDate;
}

- (IBAction)imvClickAction:(id)sender {
    
    [_delegate photoTapped:self];
}

@end

//
//  ReceiveCell.h
//  SFinder
//
//  Created by AOC on 30/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmppPacket.h"

@interface ReceiveCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblMessage;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;

- (void) setMessage:(XmppPacket *) revPacket;

@end

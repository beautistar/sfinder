//
//  ChatHeader.m
//  SFinder
//
//  Created by AOC on 30/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ChatHeader.h"

@implementation ChatHeader

@synthesize lblDate;

- (void) awakeFromNib {
    
    //init code
}

- (void) setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}

- (void) setdate:(NSString *)strDate {
    
    lblDate.text = strDate;
}

@end

//
//  ChatListCell.h
//  SFinder
//
//  Created by AOC on 28/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"

@class ChatListCell;

@protocol PhotoTapDelegate <NSObject>

-(void) photoTapped: (ChatListCell *) cell;

@end

@interface ChatListCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imvProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) id<PhotoTapDelegate> delegate;

- (void) setUserModel:(UserEntity *) _entity;

- (void) setRoomModel : (RoomEntity *) _roomEntity;

@end

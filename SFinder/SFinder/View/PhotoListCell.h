//
//  PhotoListCelll.h
//  SFinder
//
//  Created by AOC on 31/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoListCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end

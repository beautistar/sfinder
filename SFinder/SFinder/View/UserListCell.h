//
//  UserListCell.h
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"
@import AFNetworking;

@interface UserListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

- (void) setItem : (UserEntity *) user;

@end

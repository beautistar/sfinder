//
//  UserListCell.m
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "UserListCell.h"


@interface UserListCell ()


//@property (weak, nonatomic) IBOutlet UIView *vMatchStatus;
//@property (weak, nonatomic) IBOutlet UIButton *btnLike;
//@property (weak, nonatomic) IBOutlet UIImageView *imvMailVerified;
//@property (weak, nonatomic) IBOutlet UIImageView *imvOnlineState;


@end

@implementation UserListCell

- (void) setItem:(UserEntity *)user {
    
    NSString *short_address = [user._address componentsSeparatedByString:@","][0];
    _lblName.text = user._name;
    
    if (short_address.length != 0) {
        _lblDistance.text = [NSString stringWithFormat:@"%.2f Km (%@)", user._distance, short_address];
    } else {
        _lblDistance.text = [NSString stringWithFormat:@"%.2f Km", user._distance];
    }
    [_imvPhoto setImageWithURL:[NSURL URLWithString:user.getProfile] placeholderImage:[UIImage imageNamed:@"profile"]];
    
    
}

@end

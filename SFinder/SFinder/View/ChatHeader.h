//
//  ChatHeader.h
//  SFinder
//
//  Created by AOC on 30/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatHeader : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDate;

- (void) setdate : (NSString *) strDate;

@end

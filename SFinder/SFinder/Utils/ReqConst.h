//
//  ReqConst.h
//  SFinder
//
//  Created by AOC on 26/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define XMPP_RESOURCE                   @"SFinder"

#define XMPP_SERVER_URL                 @"52.28.97.125"

#define SERVER_BASE_URL                 @"http://52.28.97.125"


#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]

#define REQ_GETUSERINFO                 @"getUserInfo"
#define REQ_GETROOMINFO                 @"getRoomInfo"
#define REQ_UPLOADPHOTO                 @"uploadPhoto"
#define REQ_REGISTER                    @"register"
#define REQ_LOGIN                       @"login"
#define REQ_REGISTERMEMBER              @"registerMember"
#define REQ_GETAUTHCODE                 @"getAuthCode"
#define REQ_UPDATEPROFILE               @"updateProfile"
#define REQ_UPDATEPROFILEFORFILTER      @"updateProfileForFilter"
#define REQ_GETCODEFORUPDATE            @"getAuthCodeForUpdate"
#define REQ_GETFRIENDS                  @"getFriends"
#define REQ_GETUSERINFOBYID             @"getUserInfoById"
#define REQ_FILTER                      @"filter"


/**
 ** request parmeters
 **/

#define PARAM_ID                        @"id"
#define LATITUDE                        @"latitude"
#define LONGITUDE                       @"longitude"
#define AUTHCODE                        @"auth_code"
#define EMAIL                           @"email"


/**
 ** respond parameters
 **/

#define RES_USERINFO                    @"user_info"
#define RES_ID                          @"id"
#define RES_NAME                        @"name"
#define RES_SEX                         @"sex"
#define RES_AGE                         @"age"
#define RES_PHONE                       @"phone"
#define PASSWORD                        @"password"
#define RES_ADDRESS                     @"address"
#define RES_ABOUTME                     @"about_me"
#define RES_ISMEMBER                    @"state"
#define RES_PHOTO                       @"photo"
#define RES_FILEURL                     @"file_url"
#define RES_DISTANCE                    @"distance"
#define HEIGHT                          @"height"

#define DISTANCE_UP                     @"distance_up"
#define DISTANCE_DOWN                   @"distance_down"
#define AGE_UP                          @"age_up"
#define AGE_DOWN                        @"age_down"
#define HEIGHT_UP                       @"height_up"
#define HEIGHT_DOWN                     @"height_down"
#define FIGURE                          @"figure"
#define HAIR_COLOR                      @"hair_color"
#define ANAL                            @"anal"
#define BDSM                            @"bdsm"
#define BISEXUAL                        @"bisexual"
#define BLOW_JOB                        @"blow_job"
#define DELICATE_SEX                    @"delicate_sex"
#define DEEP_THROAT                     @"deep_throat"
#define DEVOT                           @"devot"
#define DIRTY_TALKS                     @"dirty_talks"
#define DOMINIANT                       @"dominant"
#define ESCORT_SERVICE                  @"escort_service"
#define HAND_JOB                        @"hand_job"
#define CLASSIC                         @"classic"
#define KISSES                          @"kisses"
#define LEATHER                         @"leather"
#define ROLLING_GAMES                   @"rolling_games"
#define MASTURBATION                    @"masturbation"
#define SPANISH                         @"spanish"
#define STRIPTEASE                      @"striptease"
#define WITHOUT_TABU                    @"without_tabu"
#define TRANSSEXUAL                     @"transsexual"

/**
 **  respond code
 **/

#define RES_CODE                        @"result_code"
#define CODE_SUCCESS                    0
#define CODE_EMAILEXIST                 201
#define CODE_EXISTNAME                  202
#define CODE_INVALIDEMAIL               203
#define CODE_WRONGAUTHCODE              204
#define CODE_INVALID_PHOTO              205
#define CODE_WRONGPWD                   205


#endif /* ReqConst_h */

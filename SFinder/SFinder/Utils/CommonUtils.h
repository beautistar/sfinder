//
//  CommonUtils.h
//  SFinder
//
//  Created by AOC on 09/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIImageResizeMagick.h"
#import "AppDelegate.h"
#import "Const.h"
#import "ReqConst.h"
@import AFNetworking;
#import <JLToast/JLToast-Swift.h>

@interface CommonUtils : NSObject 
+ (BOOL) isValidEmail: (NSString *) email;

+ (void) saveUserInfo;
+ (void) loadUserInfo;

+ (void) setUserState : (int) state;
+ (int) getUserState;

+ (void) setuserId : (int) idx;
+ (int) getUserId;

+ (void) setXmppId : (int) idx;
+ (int) getXmppId;

+ (void) setUserPhone : (NSString *) phone;
+ (NSString *) getUserPhone;

+ (void) setUserAddress : (NSString *) address;
+ (NSString *) getUserAddress;

+ (void) setUserEmail : (NSString *) email;
+ (NSString *) getUserEmail;

+ (void) setUserName : (NSString *) name;
+ (NSString *) getUserName;

+ (void) setUserProfile : (NSString *) profile;
+ (NSString *) getUserProfile;

+ (void) setUserAboutMe : (NSString *) aboutMe;
+ (NSString *) getUserAboutMe;

+ (void) setLastRegisterEmail : (NSString *) email;
+ (NSString *) getLastRegisterEmail;

+ (void) setPhotoUrls : (NSMutableArray *) photoUrls;
+ (NSMutableArray *) getPhotoUrls;

// for filter options
+ (void) setAge : (int) age;
+ (int) getAge;

+ (void) setHeight : (float) height;
+ (float) getHeight;

+ (void) setFigure : (NSString *) figure;
+ (NSString *) getFigure;

+ (void) setHairColor : (NSString *) hairColor;
+ (NSString *) getHairColor;

+ (void) setAnal : (int) anal;
+ (int) getAnal;

+ (void) setBdsm : (int) bdsm;
+ (int) getBdsm;

+ (void) setBisexual : (int) bisexual;
+ (int) getBisexual;

+ (void) setBlowJob : (int) blowJob;
+ (int) getBlowJob;

+ (void) setDelicateSex : (int) delicate_sex;
+ (int) getDelicateSex;

+ (void) setDeepThroat : (int) deep_throat;
+ (int) getDeepThroat;

+ (void) setDevot : (int) devot;
+ (int) getDevot;

+ (void) setDirtyTalks : (int) dirtyTalks;
+ (int) getDirtyTalks;

+ (void) setDominant : (int) dominant;
+ (int) getDominant;

+ (void) setEscortService : (int) escortService;
+ (int) getEscortService;

+ (void) setHandJob : (int) handJob;
+ (int) getHandJob;

+ (void) setClassic : (int) classic;
+ (int) getClassic;

+ (void) setKisses : (int) kisses;
+ (int) getKisses;

+ (void) setLeather : (int) leather;
+ (int) getLeather;

+ (void) setRollingGames : (int) rollingGames;
+ (int) getRollingGames;

+ (void) setMasturbation : (int) masturbation;
+ (int) getMasturbation;

+ (void) setSpanish : (int) spanish;
+ (int) getSpanish;

+ (void) setStriptease : (int) striptease;
+ (int) getStriptease;

+ (void) setWithoutTabu : (int) withoutTabu;
+ (int) getWithoutTabu;

+ (void) setTranssexual : (int) transsexual;
+ (int) getTranssexual;


// save image to file with size specification
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;

+ (void) vibrate;


@end

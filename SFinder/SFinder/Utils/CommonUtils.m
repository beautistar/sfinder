//
//  CommonUtils.m
//  SFinder
//
//  Created by AOC on 09/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CommonUtils.h"
#import <AudioToolbox/AudioToolbox.h>
#import "DBManager.h"
#import "UserDefault.h"

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
       
    }
    
    return self;
}

+ (BOOL) isValidEmail: (NSString *) email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (void) saveUserInfo {
    
    [CommonUtils setuserId:APPDELEGATE.Me._idx];
    [CommonUtils setUserState:APPDELEGATE.Me._userState];
    [CommonUtils setUserEmail:APPDELEGATE.Me._email];
    [CommonUtils setUserPhone:APPDELEGATE.Me._phone];
    [CommonUtils setUserAddress:APPDELEGATE.Me._address];
    [CommonUtils setPhotoUrls:APPDELEGATE.Me._photoUrl];
    [CommonUtils setUserName:APPDELEGATE.Me._name];
    [CommonUtils setUserAboutMe:APPDELEGATE.Me._aboutMe];
    
    //for filter option
    
    [CommonUtils setAge:APPDELEGATE.Me._age];
    [CommonUtils setHeight:APPDELEGATE.Me._height];
    [CommonUtils setFigure:APPDELEGATE.Me._figure];
    [CommonUtils setHairColor:APPDELEGATE.Me._hair_color];
    [CommonUtils setAnal:APPDELEGATE.Me._anal];
    [CommonUtils setBdsm:APPDELEGATE.Me._bdsm];
    [CommonUtils setBisexual:APPDELEGATE.Me._bisexual];
    [CommonUtils setBlowJob:APPDELEGATE.Me._blow_job];
    [CommonUtils setDelicateSex:APPDELEGATE.Me._delicate_sex];
    [CommonUtils setDeepThroat:APPDELEGATE.Me._deep_throat];
    [CommonUtils setDevot:APPDELEGATE.Me._devot];
    [CommonUtils setDirtyTalks:APPDELEGATE.Me._dirty_talks];
    [CommonUtils setDominant:APPDELEGATE.Me._dominant];
    [CommonUtils setEscortService:APPDELEGATE.Me._escort_service];
    [CommonUtils setHandJob:APPDELEGATE.Me._hand_job];
    [CommonUtils setClassic:APPDELEGATE.Me._classic];
    [CommonUtils setKisses:APPDELEGATE.Me._kisses];
    [CommonUtils setLeather:APPDELEGATE.Me._leather];
    [CommonUtils setRollingGames:APPDELEGATE.Me._rolling_games];
    [CommonUtils setMasturbation:APPDELEGATE.Me._masturbation];
    [CommonUtils setSpanish:APPDELEGATE.Me._spanish];
    [CommonUtils setStriptease:APPDELEGATE.Me._stripetease];
    [CommonUtils setWithoutTabu:APPDELEGATE.Me._without_tabu];
    [CommonUtils setTranssexual:APPDELEGATE.Me._transsexuel];
}

+ (void) loadUserInfo {
    
    if (APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserId];
        APPDELEGATE.Me._userState = [CommonUtils getUserState];
        APPDELEGATE.Me._photoUrl = [CommonUtils getPhotoUrls];
        APPDELEGATE.Me._address = [CommonUtils getUserAddress];
        APPDELEGATE.Me._phone = [CommonUtils getUserPhone];
        APPDELEGATE.Me._email = [CommonUtils getUserEmail];
        APPDELEGATE.Me._name = [CommonUtils getUserName];
        APPDELEGATE.Me._aboutMe = [CommonUtils getUserAboutMe];
        
        // for filter option
        APPDELEGATE.Me._age = [CommonUtils getAge];
        APPDELEGATE.Me._height = [CommonUtils getHeight];
        APPDELEGATE.Me._figure = [CommonUtils getFigure];
        APPDELEGATE.Me._hair_color = [CommonUtils getHairColor];
        APPDELEGATE.Me._anal = [CommonUtils getAnal];
        APPDELEGATE.Me._bdsm = [CommonUtils getBdsm];
        APPDELEGATE.Me._bisexual = [CommonUtils getBisexual];
        APPDELEGATE.Me._blow_job = [CommonUtils getBlowJob];
        APPDELEGATE.Me._delicate_sex = [CommonUtils getDelicateSex];
        APPDELEGATE.Me._deep_throat = [CommonUtils getDeepThroat];
        APPDELEGATE.Me._devot = [CommonUtils getDevot];
        APPDELEGATE.Me._dirty_talks = [CommonUtils getDirtyTalks];
        APPDELEGATE.Me._dominant = [CommonUtils getDominant];
        APPDELEGATE.Me._escort_service = [CommonUtils getEscortService];
        APPDELEGATE.Me._hand_job = [CommonUtils getHandJob];
        APPDELEGATE.Me._classic = [CommonUtils getClassic];
        APPDELEGATE.Me._kisses = [CommonUtils getKisses];
        APPDELEGATE.Me._leather = [CommonUtils getLeather];
        APPDELEGATE.Me._rolling_games = [CommonUtils getRollingGames];
        APPDELEGATE.Me._masturbation = [CommonUtils getMasturbation];
        APPDELEGATE.Me._spanish = [CommonUtils getSpanish];
        APPDELEGATE.Me._stripetease = [CommonUtils getStriptease];
        APPDELEGATE.Me._without_tabu = [CommonUtils getWithoutTabu];
        APPDELEGATE.Me._transsexuel = [CommonUtils getTranssexual];
     }
}

+ (void) setuserId:(int)idx {
    
    [UserDefault setIntValue:PREFKEY_USERID value:idx];
}

+ (int) getUserId {
    
    return [UserDefault getIntValue:PREFKEY_USERID];
}

+ (void) setUserState:(int)state {
    
    [UserDefault setIntValue:PREFKEY_USERSTATE value:state];
}

+ (int) getUserState {
    
    return [UserDefault getIntValue:PREFKEY_USERSTATE];
}

+ (void) setXmppId:(int)idx {
    
    [UserDefault setIntValue:PREFKEY_XMPPID value:idx];
}

+ (int) getXmppId {
    
    return [UserDefault getIntValue:PREFKEY_XMPPID];
}

+ (void) setUserPhone : (NSString *) phone {
    
    [UserDefault setStringValue:PREFKEY_USERPHONE value:phone];
}

+ (NSString *) getUserPhone {
    
    return [UserDefault getStringValue:PREFKEY_USERPHONE];
}

+ (void) setUserAddress : (NSString *) address {

    [UserDefault setStringValue:PREFKEY_USERADDRESS value:address];
}

+ (NSString *) getUserAddress {
    
    return [UserDefault getStringValue:PREFKEY_USERADDRESS];
    
}

+ (void) setUserEmail : (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_USEREMAIL value:email];
    
}

+ (NSString *) getUserEmail {
    
    return [UserDefault getStringValue:PREFKEY_USEREMAIL];
    
}

+ (void) setUserProfile : (NSString *) profile {
    
    [UserDefault setStringValue:PREFKEY_USERPHOTO value:profile];
    
}

+ (NSString *) getUserProfile {
    
    return [UserDefault getStringValue:PREFKEY_USERPHOTO];
    
}

+ (void) setUserName : (NSString *) name {
    
    [UserDefault setStringValue:PREFKEY_USERNAME value:name];
}

+ (NSString *) getUserName {
    
    return [UserDefault getStringValue:PREFKEY_USERNAME];
}

+ (void) setUserAboutMe:(NSString *)aboutMe {
    
    [UserDefault setStringValue:PREFKEY_USERABOUTME value:aboutMe];
}

+ (NSString *) getUserAboutMe {
    
    return [UserDefault getStringValue:PREFKEY_USERABOUTME];
}

+ (void) setLastRegisterEmail : (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_LASTLOGINID value:email];
    
}

+ (NSString *) getLastRegisterEmail {
    
    return [UserDefault getStringValue:PREFKEY_LASTLOGINID];
    
}

+ (void) setPhotoUrls : (NSMutableArray *) photoUrls {
    
    [UserDefault setArrayValue:PREFKEY_PHOTOURLS value:photoUrls];    
}

+ (NSMutableArray *) getPhotoUrls {
    
    return [UserDefault getArrayValue:PREFKEY_PHOTOURLS];
}

// for filter options
+ (void) setAge : (int) age {
    [UserDefault setIntValue:PREFKEY_AGE value:age];
}
+ (int) getAge {
    return [UserDefault getIntValue:PREFKEY_AGE];
}

+ (void) setHeight : (float) height {
    [UserDefault setFloatValue:PREFKEY_HEIGHT value:height];
}
+ (float) getHeight {
    return [UserDefault getFloatValue:PREFKEY_HEIGHT];
}

+ (void) setFigure : (NSString *) figure {
    [UserDefault setStringValue:PREFKEY_FIGURE value:figure];
}
+ (NSString *) getFigure {
    
    return [UserDefault getStringValue:PREFKEY_FIGURE];
}

+ (void) setHairColor : (NSString *) hairColor {
    [UserDefault setStringValue:PREFKEY_HAIRCOLOR value:hairColor];
}
+ (NSString *) getHairColor {
    
    return [UserDefault getStringValue:PREFKEY_HAIRCOLOR];
}

+ (void) setAnal : (int) anal {
    
    [UserDefault setIntValue:PREFKEY_ANAL value:anal];
}
+ (int) getAnal {
    
    return [UserDefault getIntValue:PREFKEY_ANAL];
}

+ (void) setBdsm : (int) bdsm {
    
    [UserDefault setIntValue:PREFKEY_BDSM value:bdsm];
}
+ (int) getBdsm {
    return [UserDefault getIntValue:PREFKEY_BDSM];
}

+ (void) setBisexual : (int) bisexual {
    [UserDefault setIntValue:PREFKEY_BISEXUAL value:bisexual];
}
+ (int) getBisexual {
    return [UserDefault getIntValue:PREFKEY_BISEXUAL];
}

+ (void) setBlowJob : (int) blowJob {
    [UserDefault setIntValue:PREFKEY_BLOWJOB value:blowJob];
}
+ (int) getBlowJob {
    return [UserDefault getIntValue:PREFKEY_BLOWJOB];
}

+ (void) setDelicateSex : (int) delicate_sex {
    [UserDefault setIntValue:PREFKEY_DELICATESEX value:delicate_sex];
}
+ (int) getDelicateSex {
    return [UserDefault getIntValue:PREFKEY_DELICATESEX];
}

+ (void) setDeepThroat : (int) deep_throat {
    
    [UserDefault setIntValue:PREFKEY_DEEPTHROAT value:deep_throat];
}
+ (int) getDeepThroat {
    
    return [UserDefault getIntValue:PREFKEY_DEEPTHROAT];
}

+ (void) setDevot : (int) devot {
    
    [UserDefault setIntValue:PREFKEY_DEVOT value:devot];
}
+ (int) getDevot {
    
    return [UserDefault getIntValue:PREFKEY_DEVOT];
}

+ (void) setDirtyTalks : (int) dirtyTalks {
    
    [UserDefault setIntValue:PREFKEY_DIRTYTALKS value:dirtyTalks];
}
+ (int) getDirtyTalks {
    return [UserDefault getIntValue:PREFKEY_DIRTYTALKS];
}

+ (void) setDominant : (int) dominant {
    
    [UserDefault setIntValue:PREFKEY_DOMINANT value:dominant];
}
+ (int) getDominant {
    
    return [UserDefault getIntValue:PREFKEY_DOMINANT];
}

+ (void) setEscortService : (int) escortService {

    [UserDefault setIntValue:PREFKEY_ESCORTSERVICE value:escortService];
}

+ (int) getEscortService {
    
    return [UserDefault getIntValue:PREFKEY_ESCORTSERVICE];
}

+ (void) setHandJob : (int) handJob {
    
    [UserDefault setIntValue:PREFKEY_HANDJOB value:handJob];
}
+ (int) getHandJob {
    
    return [UserDefault getIntValue:PREFKEY_HANDJOB];
}

+ (void) setClassic : (int) classic {
    
    [UserDefault setIntValue:PREFKEY_CLASSIC value:classic];
}
+ (int) getClassic {
    return [UserDefault getIntValue:PREFKEY_CLASSIC];
}

+ (void) setKisses : (int) kisses {
    [UserDefault setIntValue:PREFKEY_KISSES value:kisses];
}
+ (int) getKisses {
    
    return [UserDefault getIntValue:PREFKEY_KISSES];
}

+ (void) setLeather : (int) leather {
    
    [UserDefault setIntValue:PREFKEY_LEATHER value:leather];
}
+ (int) getLeather {
    
    return [UserDefault getIntValue:PREFKEY_LEATHER];
}

+ (void) setRollingGames : (int) rollingGames {
    
    [UserDefault setIntValue:PREFKEY_ROLLINGGAMES value:rollingGames];
}
+ (int) getRollingGames {
    
    return [UserDefault getIntValue:PREFKEY_ROLLINGGAMES];
}

+ (void) setMasturbation : (int) masturbation {
    [UserDefault setIntValue:PREFKEY_MASTURBATION value:masturbation];
}
+ (int) getMasturbation {
    return [UserDefault getIntValue:PREFKEY_MASTURBATION];
}

+ (void) setSpanish : (int) spanish {
    [UserDefault setIntValue:PREFKEY_SPANISH value:spanish];
}
+ (int) getSpanish {
    
    return [UserDefault getIntValue:PREFKEY_SPANISH];
}

+ (void) setStriptease : (int) striptease {
    [UserDefault setIntValue:PREFKEY_STRIPTEASE value:striptease];
}
+ (int) getStriptease {
    return [UserDefault getIntValue:PREFKEY_STRIPTEASE];
}

+ (void) setWithoutTabu : (int) withoutTabu {
    [UserDefault setIntValue:PREFKEY_WITHOUTTABU value:withoutTabu];
}
+ (int) getWithoutTabu {
    return [UserDefault getIntValue:PREFKEY_WITHOUTTABU];
}

+ (void) setTranssexual : (int) transsexual {
    [UserDefault setIntValue:PREFKEY_TRANSSEXUAL value:transsexual];
}
+ (int) getTranssexual {
    return [UserDefault getIntValue:PREFKEY_TRANSSEXUAL];
}


// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage {
    
    return [srcImage resizedImageByMagick:@"256x256#"];
}

+ (UIImage *) imageResizeforChat: (UIImage *) scrImage {
    
    return [scrImage resizedImageByMagick:@"256x256#"];
}

// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    if(isProfile) {
        
        int milliseconds = [NSDate timeIntervalSinceReferenceDate]*1000.0 / 1000;
        
        outputFileName = [NSString stringWithFormat:@"%@%d%@", @"profile", milliseconds, @".jpg"];
        outputImage = [CommonUtils imageResize:srcImage];
        
    }
    else {
        
        outputFileName = @"chatfile.jpg";
        outputImage = [CommonUtils imageResizeforChat:srcImage];
    }
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    //    if(isProfile) {
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    //    } else {
    //        [UIImageJPEGRepresentation(outputImage, 0.8) writeToFile:filePath atomically:YES];
    //    }
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) vibrate {
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end

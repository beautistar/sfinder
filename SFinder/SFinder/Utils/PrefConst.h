//
//  PrefConst.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_USERID                      @"userid"

#define PREFKEY_USEREMAIL                   @"user_email"
#define PREFKEY_USERPHONE                   @"user_phone"
#define PREFKEY_USERNAME                    @"username"
#define PREFKEY_USERSTATE                   @"user_state"
#define PREFKEY_USERPHOTO                   @"user_photo"
#define PREFKEY_USERADDRESS                 @"user_address"
#define PREFKEY_USERABOUTME                 @"user_aboutMe"
#define PREFKEY_PHOTOURLS                   @"user_photourls"

//for filter opttion

#define PREFKEY_AGE                         @"age"
#define PREFKEY_HEIGHT                      @"height"
#define PREFKEY_FIGURE                      @"figure"
#define PREFKEY_HAIRCOLOR                   @"hair_color"
#define PREFKEY_ANAL                        @"anal"
#define PREFKEY_BDSM                        @"bdsm"
#define PREFKEY_BISEXUAL                    @"bisexual"
#define PREFKEY_BLOWJOB                     @"blow_job"
#define PREFKEY_DELICATESEX                 @"delicate_sex"
#define PREFKEY_DEEPTHROAT                  @"deep_throat"
#define PREFKEY_DEVOT                       @"devot"
#define PREFKEY_DIRTYTALKS                  @"dirty_talks"
#define PREFKEY_DOMINANT                    @"dominant"
#define PREFKEY_ESCORTSERVICE               @"escort_service"
#define PREFKEY_HANDJOB                     @"hand_job"
#define PREFKEY_CLASSIC                     @"classic"
#define PREFKEY_KISSES                      @"kisses"
#define PREFKEY_LEATHER                     @"leather"
#define PREFKEY_ROLLINGGAMES                @"rolling_games"
#define PREFKEY_MASTURBATION                @"masturbation"
#define PREFKEY_SPANISH                     @"spanish"
#define PREFKEY_STRIPTEASE                  @"striptease"
#define PREFKEY_WITHOUTTABU                 @"without_tabu"
#define PREFKEY_TRANSSEXUAL                 @"transsexual"


#define PREFKEY_XMPPID                      @"XMPPID"

#define PREFKEY_LASTLOGINID                 @"lastlogin_id"

#define PREFKEY_USER_ALLOW_PUSH             @"allowed_push_notification"
#define PREFKEY_USER_ALLOW_ADD_FRIEND       @"allowed_add_friend"

#define PREFKEY_DEVICE_TOKEN                @"device_token"


#endif /* PrefConst_h */

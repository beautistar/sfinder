//
//  Const.h
//  SFinder
//
//  Created by AOC on 22/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Const : NSObject

#define SAVE_ROOT_PATH                      @"SFinder"



extern int BACK_WHERE;
extern int _from;
extern int deviceType;



// int const

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

// string const

#define KEY_TIME_SEPERTATOR                     @"#"
#define KEY_FILE_MARKER                         @"FILE#"
#define KEY_IMAGE_MARKER                        @"IMAGE#"
#define KEY_VIDEO_MARKER                        @"VIDEO#"
#define KEY_ROOM_MARKER                         @"ROOM#"

#define TIME_AM                                 @"AM"
#define TIME_PM                                 @"PM"

#define STATE_UNREGISTERED                      0
#define STATE_REGISTERED                        1
#define STATE_MEMBER                            2
#define STATE_ADMIN                             3

#define FROM_RADAR                              111
#define FROM_PEOPLE                             222

#define FROM_TAKEPHOTO                          333

#define XMPP_PASSWORD                           @"sfinder"

extern int _from;



extern BOOL bEnteredBackground;                 // when the app is entering in a background, it wil be set as a true.
// after the app is entering in a foreground, it will be set as a false.

extern BOOL bIsShownChatView;                   // if the current topviewcontroller is ChatViewController, true

extern BOOL bIsShownChatList;                   // the current topviewcontroller is ChatListViewController, true

extern NSString *photoPath;

@end

/*
 
 The MIT License (MIT)
 
 Copyright (c) 2015 ABM Adnan
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

#import "Dot.h"
@import AFNetworking;

@implementation Dot

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setOpaque:NO];
        
        self.layer.cornerRadius = frame.size.width / 2;
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void) setImage:(UIImage *)image {
    
    UIImageView *imv = [[UIImageView alloc] initWithFrame:self.frame];
//    imv.layer.cornerRadius = self.frame.size.width / 2;
    imv.image = image;
    [self addSubview:imv];    
}

- (void) setImageWithUrl:(NSString *) url {
    
    UIImageView *imv = [[UIImageView alloc] initWithFrame:self.frame];
    //    imv.layer.cornerRadius = self.frame.size.width / 2;
    [imv setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"profile"]];
    [self addSubview:imv];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect{
    // Drawing code
    [self.color setFill];
    
    UIBezierPath *dotPath = [UIBezierPath bezierPathWithOvalInRect:
                             CGRectMake(rect.size.width / 8, rect.size.height / 8, rect.size.width/4*3, rect.size.height/4*3)];
    
    // Fill the path
    [dotPath fill];
}

@end

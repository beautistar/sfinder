//
//  AppDelegate.h
//  SFinder
//
//  Created by AOC on 17/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "PCAngularActivityIndicatorView.h"
#import "Reachability.h"
#import "ReqConst.h"
//#import "UITabbar+CustomBadge.h"
//#import "LNNotificationsUI.h"
#import "DBManager.h"
#import "UserEntity.h"
#import "XmppEndPoint.h"
#import "ChatMainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UIView* baseView;
    
    UserEntity *Me;
    XmppEndPoint * xmpp;
    
    NSString * _deviceToken;
    
    UITabBar * gTabbar;
    
    NSDictionary *locationString;
}

@property (strong, nonatomic) UIWindow *window;
//@property (nonatomic, strong) PCAngularActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UserEntity *Me;
@property (nonatomic, strong) XmppEndPoint * xmpp;
@property (nonatomic, strong) NSString * _deviceToken;
@property (nonatomic, retain) UITabBar * gTabbar;
@property (nonatomic, strong) NSDictionary * locationString;

@property (nonatomic, strong) ChatMainViewController *chatVC;

@property BOOL started;

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;

// check the current network state
-(BOOL) connected;

- (void) notifyReceiveNewMessage;


@end


//
//  MapViewController.m
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MapViewController.h"
#import "FullProfileViewController.h"

#import <MapKit/MapKit.h>
#import "CommonUtils.h"
#import "ChattingViewController.h"
#import "DCMessageDelegate.h"


@interface MapViewController () <MKMapViewDelegate, XmppFriendRequestDelegate> {
    
    __weak IBOutlet UIImageView *imvPhoto;
    __weak IBOutlet UITextView *tvAboutMe;
    
    UserEntity * _user;
    RoomEntity * _requestRoom;
}

@property (weak, nonatomic) IBOutlet UIView *vUserInfo;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) MKPolyline *routeLine;
@property (nonatomic, strong) MKPolylineView *routeLineView;


@end

@implementation MapViewController

@synthesize _selectedUser;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    [self initMap];
   
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    //set user info
    
    if (self._from == FROM_PEOPLE  && _user._idx != _selectedUser._idx) {
        
        [self.vUserInfo setHidden:NO];
        
    } else if (self._from == FROM_RADAR && _user._userState == STATE_UNREGISTERED) {
        
        [self.vUserInfo setHidden:YES];
    }
    
    APPDELEGATE.xmpp._friendRequestDelegate = self;
    
    self.navigationItem.title = _selectedUser._name;
    if (_user._idx == _selectedUser._idx) {
        self.navigationItem.title = @"Me";
    }
    [imvPhoto setImageWithURL:[NSURL URLWithString:_selectedUser.getProfile] placeholderImage:[UIImage imageNamed:@"profile"]];
    tvAboutMe.text = _selectedUser._aboutMe;
}

- (void ) viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}

- (void) initMap {
    
    /*
    
     1.
    mapView.delegate = self
    
     2.
    let sourceLocation = CLLocationCoordinate2D(latitude: 40.759011, longitude: -73.984472)
    let destinationLocation = CLLocationCoordinate2D(latitude: 40.748441, longitude: -73.985564)
    
     3.
    let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
    let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
    
     4.
    let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
    let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
    
     5.
    let sourceAnnotation = MKPointAnnotation()
    sourceAnnotation.title = "Times Square"
    
    if let location = sourcePlacemark.location {
        sourceAnnotation.coordinate = location.coordinate
    }
    
    
    let destinationAnnotation = MKPointAnnotation()
    destinationAnnotation.title = "Empire State Building"
    
    if let location = destinationPlacemark.location {
        destinationAnnotation.coordinate = location.coordinate
    }
    
     6.
    self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
    
     7.
    let directionRequest = MKDirectionsRequest()
    directionRequest.source = sourceMapItem
    directionRequest.destination = destinationMapItem
    directionRequest.transportType = .Automobile
    
    // Calculate the direction
    let directions = MKDirections(request: directionRequest)
    
     8.
    directions.calculateDirectionsWithCompletionHandler {
        (response, error) -> Void in
        
        guard let response = response else {
            if let error = error {
                print("Error: \(error)")
            }
            
            return
        }
        
        let route = response.routes[0]
        self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.AboveRoads)
        
        let rect = route.polyline.boundingMapRect
        self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
    }
    
    */
    
    _mapView.delegate = self;
    
    CLLocationCoordinate2D sourceLocation = CLLocationCoordinate2DMake(_user._latitude, _user._longitude);
    CLLocationCoordinate2D destinationLocation = CLLocationCoordinate2DMake(_selectedUser._latitude, _selectedUser._longitude);
    
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceLocation addressDictionary:nil];
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destinationLocation addressDictionary:nil];
    
    MKMapItem *sourceMapItem = [[MKMapItem alloc] initWithPlacemark:sourcePlacemark];
    MKMapItem *destinationMapItem = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    
    MKPointAnnotation *sourceAnnotation = [[MKPointAnnotation alloc] init];
    [sourceAnnotation setTitle:_user._address];
    [sourceAnnotation setCoordinate:sourcePlacemark.location.coordinate];
   
    MKPointAnnotation *destinationAnnotation = [[MKPointAnnotation alloc] init];
    [destinationAnnotation setTitle:_selectedUser._address];
    [destinationAnnotation setCoordinate:destinationPlacemark.location.coordinate];
    
    [_mapView showAnnotations:@[sourceAnnotation, destinationAnnotation] animated:YES];

    MKDirectionsRequest *directionRequest = [[MKDirectionsRequest alloc] init];
    directionRequest.source = sourceMapItem;
    directionRequest.destination = destinationMapItem;
    directionRequest.transportType = MKDirectionsTransportTypeAutomobile;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionRequest];
    
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
        if (response) {
            MKRoute *route = [response.routes firstObject];
            [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
            
            MKMapRect rect = route.polyline.boundingMapRect;
            [self.mapView setRegion:MKCoordinateRegionForMapRect(rect) animated:YES];
            
            
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
    }];
}

#pragma mark - mkmapview delegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor redColor];
    renderer.lineWidth = 4;
    return renderer;
}


- (IBAction)gotoFullDescription:(id)sender {
    
//    PeopleProfileViewController *profileVC = (PeopleProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"PeopleProfileViewController"];
//    
//    profileVC._fullUser = _selectedUser;
//    
//    [self.navigationController pushViewController:profileVC animated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SegueToProfile"]) {
        
        FullProfileViewController *profileVC = (FullProfileViewController *) [segue destinationViewController];
        profileVC._fullUser = _selectedUser;
    }
}

- (IBAction)closeAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)messageAction:(id)sender {
    
    // pop up FriendProfileViewController and the push ChatViewController
    //get the existing navigationController view stack
    NSMutableArray* newViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    //drop the last viewController and replace it with the new one!
    ChattingViewController *chatVC = (ChattingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChattingViewController"];
    chatVC.hidesBottomBarWhenPushed = YES;
    [newViewControllers replaceObjectAtIndex:newViewControllers.count-1 withObject:chatVC];
    
    RoomEntity *_chatRoom = [self makeRoomWithFriend];
    [APPDELEGATE.xmpp EnterChattingRoom:_chatRoom];
    chatVC.chatRoom = _chatRoom;
    
    //set the new view Controllers in the navigationController Stack
    [self.navigationController setViewControllers:newViewControllers animated:YES];
    
}

// enter a room to sent a request
- (void) gotoAddFriendRoom {
    
    _requestRoom = [self makeRoomWithFriend];
    
    // enter a room
    [APPDELEGATE.xmpp enterChattingRoomForFriendRequest:_requestRoom];
}

// friend request delegate
- (void) sendFriendRequest {
    
    XmppPacket *_sendPacket = [self makePacket:_TEXT send:[NSString stringWithFormat:@"%@ %@",_user._name, [APPDELEGATE.locationString objectForKey:@"ADDFRIEND_MSG"]] sendFile:@""];
    
    [APPDELEGATE.xmpp sendPacket:_sendPacket];
    
    UserEntity *_participant = _requestRoom._participants[0];
    [APPDELEGATE.xmpp sendPacket:_sendPacket to:_participant._idx];
    
    APPDELEGATE.xmpp.isSentFriendRequest = NO;
}


// ----------------------------------------------
//  make sending / receiving packet
//  sending   : isSend - YES
//  receiving : isSend - NO
//
- (XmppPacket *) makePacket:(ChatType) _type send:(NSString *) _sendString sendFile:(NSString*) fileName {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components
    
    [components month];     //  month
    [components day];       //  day
    [components year];      //  year
    
    NSString *  year_month_day = [NSString stringWithFormat:@"%d%02d%02d", (int)[components year], (int)[components month] , (int)[components day]];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"H:mm:ss"];
    NSString * currentTime = [formatter stringFromDate:[NSDate date]];
    
    NSString * sendTime = [NSString stringWithFormat:@"%@,%@", year_month_day, currentTime];
    
    // ------------------------------
    // _from : sender idx
    XmppPacket *_sendPacket = [[XmppPacket alloc] initWithType:_requestRoom._name participantName:_requestRoom._participantsName senderName:_user._name chatType:_type body:_sendString fileName:fileName sendTime:sendTime];
    
    return _sendPacket;
}


- (RoomEntity *) makeRoomWithFriend {
    
    // make a chatting room with selected friend
    NSMutableArray *_friends = [NSMutableArray array];
    [_friends addObject:_selectedUser];
    
    // new chatting room
    RoomEntity *_chatRoom = [[RoomEntity alloc] initWithParticipants:_friends];
    
    // check user's room list, if user has this room or not
    for(RoomEntity * _existEntity in _user._roomList) {
        
        if([_existEntity equals:_chatRoom]) {
            _chatRoom = _existEntity;
            return _chatRoom;
        }
    }
    
    XmppPacket *_sendPacket = [self makePacket:_TEXT send:[NSString stringWithFormat:@"%@ %@",_user._name, [APPDELEGATE.locationString objectForKey:@"ADDFRIEND_MSG"]] sendFile:@""];

    
    _chatRoom._recentContent = _sendPacket._bodyString;
//    _chatRoom._recentDate = _sendPacket._timestamp;
    _chatRoom._recentDate = [_sendPacket getDisplayTime:_sendPacket._timestamp];
    
    // if user has not the current room, add it into user's room list
    [_user._roomList addObject:_chatRoom];
    
    // update db
    [[DBManager getSharedInstance] saveRoomWithName:_chatRoom._name participant_name:_chatRoom._participantsName recent_message:_chatRoom._recentContent recent_time:_chatRoom._recentDate recent_counter:_chatRoom._recentCounter];
    
    return _chatRoom;
}




@end

//
//  TermsViewController.m
//  SFinder
//
//  Created by AOC on 06/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "TermsViewController.h"

@implementation TermsViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end

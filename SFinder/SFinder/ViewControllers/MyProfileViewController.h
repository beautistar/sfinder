//
//  MyProfileViewController.h
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MyProfileViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray *arrPhotoPath;
@end

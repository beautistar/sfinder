//
//  IntroViewController.m
//  SFinder
//
//  Created by AOC on 22/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "IntroViewController.h"
#import "CommonUtils.h"

@interface IntroViewController () {

    UserEntity *_user;
    
    CGFloat latitude, longitude;
    
    int _nReqCounter;
    BOOL isEnglish;
}

@end

@implementation IntroViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    _user = APPDELEGATE.Me;
    isEnglish = YES;

    NSString *loc = [APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"];
    NSLog(@"loc = %@", loc);
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)enterAction:(id)sender {
    
    NSLog(@"address : %@", _user._address);
    
    if (_user._latitude != 0.0 || _user._address.length != 0) {
        
        [self getUserInfo];
    }
}

- (void) gotoMain {
    
    UITabBarController *mainVC = (UITabBarController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabViewController"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:mainVC];
}

- (void) getUserInfo {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETUSERINFO];
    
    NSDictionary * params = @{
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude]
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
//    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
//        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [_user._userList removeAllObjects];
        
        NSLog(@"userinfo respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSMutableArray *usersDict = [responseObject valueForKey:RES_USERINFO];
            
            for (NSDictionary *dict in usersDict) {
                
                UserEntity *other = [[UserEntity alloc] init];
            
                other._idx = [[dict valueForKey:RES_ID] intValue];
                other._name = [dict valueForKey:RES_NAME];
                other._latitude = [[dict valueForKey:LATITUDE] floatValue];
                other._longitude = [[dict valueForKey:LONGITUDE] floatValue];
                other._address = [dict valueForKey:RES_ADDRESS];
                other._sex = [[dict valueForKey:RES_SEX] intValue];
                other._aboutMe = [dict valueForKey:RES_ABOUTME];
                other._userState = [[dict valueForKey:RES_ISMEMBER] intValue];
                other._phone = [dict valueForKey:RES_PHONE];                
                
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                
                photoUrl = [dict valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [other._photoUrl addObject:photo_];
                }
//                other._photoUrl = [dict valueForKey:RES_PHOTO];
                other._distance = [[dict valueForKey:RES_DISTANCE] floatValue];
                
                //for filter options
                other._age = [[dict valueForKey:RES_AGE] intValue];
                other._height = [[dict valueForKey:HEIGHT] floatValue];
                other._figure = [dict valueForKey:FIGURE];
                other._hair_color = [dict valueForKey:HAIR_COLOR];
                other._anal = [[dict valueForKey:ANAL] intValue];
                other._bdsm = [[dict valueForKey:BDSM] intValue];
                other._bisexual = [[dict valueForKey:BISEXUAL] intValue];
                other._blow_job = [[dict valueForKey:BLOW_JOB] intValue];
                other._delicate_sex = [[dict valueForKey:DELICATE_SEX] intValue];
                other._deep_throat = [[dict valueForKey:DEEP_THROAT] intValue];
                other._devot = [[dict valueForKey:DEVOT] intValue];
                other._dirty_talks = [[dict valueForKey:DIRTY_TALKS] intValue];
                other._dominant = [[dict valueForKey:DOMINIANT] intValue];
                other._escort_service = [[dict valueForKey:ESCORT_SERVICE] intValue];
                other._hand_job = [[dict valueForKey:HAND_JOB] intValue];
                other._classic = [[dict valueForKey:CLASSIC] intValue];
                other._kisses = [[dict valueForKey:KISSES] intValue];
                other._leather = [[dict valueForKey:LEATHER] intValue];
                other._rolling_games = [[dict valueForKey:ROLLING_GAMES] intValue];
                other._masturbation = [[dict valueForKey:MASTURBATION] intValue];
                other._spanish = [[dict valueForKey:SPANISH] intValue];
                other._stripetease = [[dict valueForKey:STRIPTEASE] intValue];
                other._without_tabu = [[dict valueForKey:WITHOUT_TABU] intValue];
                other._transsexuel = [[dict valueForKey:TRANSSEXUAL] intValue];
                
            
                
                if (_user._idx != other._idx) {
                
                    [_user._userList addObject:other];
                }
            }
        }
        
        [CommonUtils loadUserInfo];
        if ([_user isValid]) {
//            [self getFriend];
            [self loadRoomInfo];
        } else {
            [self hideLoadingView];
            [self gotoMain];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
}

- (void) getFriend {
    
    if(_user._friendList != nil)
        [_user._friendList removeAllObjects];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETFRIENDS];
    
    NSDictionary * params = @{RES_ID : [NSNumber numberWithInteger:_user._idx],
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude]
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"userinfo respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSMutableArray *usersDict = [responseObject valueForKey:RES_USERINFO];
            
            for (NSDictionary *dict in usersDict) {
                
                UserEntity *friend = [[UserEntity alloc] init];
                
                friend._idx = [[dict valueForKey:RES_ID] intValue];
                friend._name = [dict valueForKey:RES_NAME];
                friend._latitude = [[dict valueForKey:LATITUDE] floatValue];
                friend._longitude = [[dict valueForKey:LONGITUDE] floatValue];
                friend._address = [dict valueForKey:RES_ADDRESS];
                friend._sex = [[dict valueForKey:RES_SEX] intValue];
                friend._age = [[dict valueForKey:RES_AGE] intValue];
                friend._aboutMe = [dict valueForKey:RES_ABOUTME];
                friend._userState = [[dict valueForKey:RES_ISMEMBER] intValue];
                
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                photoUrl = [dict valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [friend._photoUrl addObject:photo_];
                }
                friend._phone = [dict valueForKey:RES_PHONE];
//                friend._photoUrl = [dict valueForKey:RES_PHOTO];
                friend._distance = [[dict valueForKey:RES_DISTANCE] floatValue];
                
                //for filter options
                friend._age = [[dict valueForKey:RES_AGE] intValue];
                friend._height = [[dict valueForKey:HEIGHT] floatValue];
                friend._figure = [dict valueForKey:FIGURE];
                friend._hair_color = [dict valueForKey:HAIR_COLOR];
                friend._anal = [[dict valueForKey:ANAL] intValue];
                friend._bdsm = [[dict valueForKey:BDSM] intValue];
                friend._bisexual = [[dict valueForKey:BISEXUAL] intValue];
                friend._blow_job = [[dict valueForKey:BLOW_JOB] intValue];
                friend._delicate_sex = [[dict valueForKey:DELICATE_SEX] intValue];
                friend._deep_throat = [[dict valueForKey:DEEP_THROAT] intValue];
                friend._devot = [[dict valueForKey:DEVOT] intValue];
                friend._dirty_talks = [[dict valueForKey:DIRTY_TALKS] intValue];
                friend._dominant = [[dict valueForKey:DOMINIANT] intValue];
                friend._escort_service = [[dict valueForKey:ESCORT_SERVICE] intValue];
                friend._hand_job = [[dict valueForKey:HAND_JOB] intValue];
                friend._classic = [[dict valueForKey:CLASSIC] intValue];
                friend._kisses = [[dict valueForKey:KISSES] intValue];
                friend._leather = [[dict valueForKey:LEATHER] intValue];
                friend._rolling_games = [[dict valueForKey:ROLLING_GAMES] intValue];
                friend._masturbation = [[dict valueForKey:MASTURBATION] intValue];
                friend._spanish = [[dict valueForKey:SPANISH] intValue];
                friend._stripetease = [[dict valueForKey:STRIPTEASE] intValue];
                friend._without_tabu = [[dict valueForKey:WITHOUT_TABU] intValue];
                friend._transsexuel = [[dict valueForKey:TRANSSEXUAL] intValue];
                
                [_user._friendList addObject:friend];
            }
        }
        
        [self loadRoomInfo];
        

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
}

- (void) loadRoomInfo {
    
    //    init user's room list
    
    if (_user._roomList != nil) {
        [_user._roomList removeAllObjects];
    }
    
    NSArray * _arrRooms = [[DBManager getSharedInstance] loadRoom];
    
    _nReqCounter = (int)[_arrRooms count];
    
    if(_nReqCounter == 0) {
        
        [self loginToChattingServer];
        return;
    }
    
    for(int i = 0; i < [_arrRooms count]; i ++) {
        
        // get room name from database
        NSString * name = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"PARTICIPANTNAME"]];
        
        NSArray *ids = [name componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx != _user._idx) {
                
                other = idx;
                break;
            }
        }
        
        // get participants info from a server
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETUSERINFOBYID, other];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            NSLog(@"userinfo respond : %@", responseObject);
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                _entity._name = [_dictParticipant valueForKey:RES_NAME];
                _entity._latitude = [[_dictParticipant valueForKey:LATITUDE] floatValue];
                _entity._longitude = [[_dictParticipant valueForKey:LONGITUDE] floatValue];
                _entity._address = [_dictParticipant valueForKey:RES_ADDRESS];
                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
                _entity._age = [[_dictParticipant valueForKey:RES_AGE] intValue];
                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
                _entity._userState = [[_dictParticipant valueForKey:RES_ISMEMBER] intValue];
                _entity._phone = [_dictParticipant valueForKey:RES_PHONE];
                
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                photoUrl = [_dictParticipant valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [_entity._photoUrl addObject:photo_];
                }
                
//                _entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTO];
                _entity._distance = [[_dictParticipant valueForKey:RES_DISTANCE] floatValue];
                
                //for filter options
                _entity._age = [[_dictParticipant valueForKey:RES_AGE] intValue];
                _entity._height = [[_dictParticipant valueForKey:HEIGHT] floatValue];
                _entity._figure = [_dictParticipant valueForKey:FIGURE];
                _entity._hair_color = [_dictParticipant valueForKey:HAIR_COLOR];
                _entity._anal = [[_dictParticipant valueForKey:ANAL] intValue];
                _entity._bdsm = [[_dictParticipant valueForKey:BDSM] intValue];
                _entity._bisexual = [[_dictParticipant valueForKey:BISEXUAL] intValue];
                _entity._blow_job = [[_dictParticipant valueForKey:BLOW_JOB] intValue];
                _entity._delicate_sex = [[_dictParticipant valueForKey:DELICATE_SEX] intValue];
                _entity._deep_throat = [[_dictParticipant valueForKey:DEEP_THROAT] intValue];
                _entity._devot = [[_dictParticipant valueForKey:DEVOT] intValue];
                _entity._dirty_talks = [[_dictParticipant valueForKey:DIRTY_TALKS] intValue];
                _entity._dominant = [[_dictParticipant valueForKey:DOMINIANT] intValue];
                _entity._escort_service = [[_dictParticipant valueForKey:ESCORT_SERVICE] intValue];
                _entity._hand_job = [[_dictParticipant valueForKey:HAND_JOB] intValue];
                _entity._classic = [[_dictParticipant valueForKey:CLASSIC] intValue];
                _entity._kisses = [[_dictParticipant valueForKey:KISSES] intValue];
                _entity._leather = [[_dictParticipant valueForKey:LEATHER] intValue];
                _entity._rolling_games = [[_dictParticipant valueForKey:ROLLING_GAMES] intValue];
                _entity._masturbation = [[_dictParticipant valueForKey:MASTURBATION] intValue];
                _entity._spanish = [[_dictParticipant valueForKey:SPANISH] intValue];
                _entity._stripetease = [[_dictParticipant valueForKey:STRIPTEASE] intValue];
                _entity._without_tabu = [[_dictParticipant valueForKey:WITHOUT_TABU] intValue];
                _entity._transsexuel = [[_dictParticipant valueForKey:TRANSSEXUAL] intValue];
                
                [_arrParticipants addObject:_entity];
                
                // make a new room
                // set the room information with db data
                // recentContent, recenctDate, recentCounter
                NSString * roomName = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"NAME"]];
                NSString * recentContent = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTMESSAGE"]];
                NSString * recentDate = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTTIME"]];
                NSString * recentCounter = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTCOUNTER"]];
                
                RoomEntity *loadRoom = [[RoomEntity alloc] initWithName:roomName participants:_arrParticipants];
                
                loadRoom._recentContent = recentContent;
                loadRoom._recentDate = recentDate;
                loadRoom._recentCounter = [recentCounter intValue];
                
                //_user.notReadCount += loadRoom._recentCounter;
                
                [_user._roomList addObject:loadRoom];
            }
            
            if(--_nReqCounter <= 0) {                
                
                [self loginToChattingServer];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            
            if(--_nReqCounter <= 0) {
                
                [self hideLoadingView];
                
                [self loginToChattingServer];
            }
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) loginToChattingServer {
    
    NSLog(@"User idx : %d", _user._idx);
//    NSLog(@"User Password : %@", _user._password);
    
    if ([APPDELEGATE.xmpp connect:_user._idx password:XMPP_PASSWORD])
    {
        // success to connect to xmpp server
        [self onSuccess];
    }
    else
    {
        // fail to connect to xmpp server
        [self hideLoadingView];
   
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }
}

// go to main
- (void) onSuccess {
    
    [self hideLoadingView];
    
    [[DBManager getSharedInstance] updateChatNoCurrent];
    
    [self gotoMain];
}



@end

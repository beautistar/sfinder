            //
//  FilterViewController.m
//  SFinder
//
//  Created by AOC on 25/01/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import "FilterViewController.h"
@import ActionSheetPicker_3_0;
#import "CommonUtils.h"

@interface FilterViewController() <UITextFieldDelegate> {
    
    __weak IBOutlet UITextField *tfDistFrom;
    __weak IBOutlet UITextField *tfDistTo;
    __weak IBOutlet UITextField *tfAgeFrom;
    __weak IBOutlet UITextField *tfAgeTo;
    __weak IBOutlet UITextField *tfHeightFrom;
    __weak IBOutlet UITextField *tfHeightTo;
    __weak IBOutlet UITextField *tfFigure;
    __weak IBOutlet UITextField *tfHairColor;
    
    __weak IBOutlet UISwitch *swhAnal;
    __weak IBOutlet UISwitch *swhBdsm;
    __weak IBOutlet UISwitch *swhBisexual;
    __weak IBOutlet UISwitch *swhBlowJob;
    __weak IBOutlet UISwitch *swhDelicateSex;
    __weak IBOutlet UISwitch *swhDeepThroat;
    __weak IBOutlet UISwitch *swhDevot;
    __weak IBOutlet UISwitch *swhDirtyTalks;
    __weak IBOutlet UISwitch *swhDominant;
    __weak IBOutlet UISwitch *swhEscortService;
    __weak IBOutlet UISwitch *swhHandHob;
    __weak IBOutlet UISwitch *swhClassic;
    __weak IBOutlet UISwitch *swhKisses;
    __weak IBOutlet UISwitch *swhLeather;
    __weak IBOutlet UISwitch *swhRollingGames;
    __weak IBOutlet UISwitch *swhMasturbation;
    __weak IBOutlet UISwitch *swhSpanish;
    __weak IBOutlet UISwitch *swhStriptease;
    __weak IBOutlet UISwitch *swhWithoutTabu;
    __weak IBOutlet UISwitch *swhTranssexual;
    
    UserEntity *_user;
    
    int selectedFigureIdx, selectedHairColorIdx;
    
}

@end

@implementation FilterViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    selectedFigureIdx = 0;
    selectedHairColorIdx = 0;
    [self initKeyboard];
}

- (void) initKeyboard {
    
    //keybiard Done button add
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    [numberToolbar sizeToFit];
    tfDistFrom.inputAccessoryView = numberToolbar;
    tfDistTo.inputAccessoryView = numberToolbar;
    tfAgeFrom.inputAccessoryView = numberToolbar;
    tfAgeTo.inputAccessoryView = numberToolbar;
    tfHeightFrom.inputAccessoryView = numberToolbar;
    tfHeightTo.inputAccessoryView = numberToolbar;
    
    // tableview background
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background filter"]];
}

-(void)doneWithNumberPad{
    
    [tfDistFrom resignFirstResponder];
    [tfDistTo resignFirstResponder];
    [tfAgeFrom resignFirstResponder];
    [tfAgeTo resignFirstResponder];
    [tfHeightTo resignFirstResponder];
    [tfHeightFrom resignFirstResponder];
}
- (IBAction)figureAction:(id)sender {
    
     [self.view endEditing:YES];
    
    NSArray *figures = [NSArray arrayWithObjects:[APPDELEGATE.locationString objectForKey:@"SLENDER"], [APPDELEGATE.locationString objectForKey:@"SPORTY"], [APPDELEGATE.locationString objectForKey:@"CHUBBY"], [APPDELEGATE.locationString objectForKey:@"NORMAL"], [APPDELEGATE.locationString objectForKey:@"PREF"], nil];
    
    [ActionSheetStringPicker showPickerWithTitle:[APPDELEGATE.locationString objectForKey:@"SEL_FIGURE"]
                                            rows:figures
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           tfFigure.text = selectedValue;
                                           selectedFigureIdx = (int)selectedIndex;
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                         tfFigure.text = @"";
                                     }
                                          origin:sender];    
}

- (IBAction)hairColorAction:(id)sender {
    
    [self.view endEditing:YES];
    
    NSArray *colors = [NSArray arrayWithObjects:[APPDELEGATE.locationString objectForKey:@"BLACK"], [APPDELEGATE.locationString objectForKey:@"BLOND"], [APPDELEGATE.locationString objectForKey:@"BROWN"], [APPDELEGATE.locationString objectForKey:@"RED"], [APPDELEGATE.locationString objectForKey:@"CHOOSEN"], nil];
    
    [ActionSheetStringPicker showPickerWithTitle:[APPDELEGATE.locationString objectForKey:@"SEL_HAIRCOLOR"]
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           tfHairColor.text = selectedValue;
                                           selectedHairColorIdx = (int)selectedIndex;
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                         tfHairColor.text = @"";
                                     }
                                          origin:sender];
}

- (BOOL) isValid {
    
    if (tfDistFrom.text.length == 0 || tfDistTo.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_DISTANCERANCE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfAgeFrom.text.length == 0 || tfAgeTo.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_AGERANGE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfHeightFrom.text.length == 0 || tfHeightTo.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_HEIGHTRANGE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfFigure.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_FIGURE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfHairColor.text.length == 0) {
        
         [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_HAIRCOLOR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)searchAction:(id)sender {
    
//    if ([self isValid]) {
    
        [self filter];
//    }
}

- (void) filter {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_FILTER];
    
    NSString *figure, *hairColor;
    if (tfFigure.text.length == 0)  figure = @"0"; else figure = [tfFigure.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (tfHairColor.text.length == 0) hairColor = @"0"; else hairColor = [tfHairColor.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSDictionary * params = @{RES_ID : [NSNumber numberWithInt:_user._idx],
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
                              DISTANCE_UP : [NSNumber numberWithInt:[tfDistTo.text intValue]],
                              DISTANCE_DOWN : [NSNumber numberWithInt:[tfDistFrom.text intValue]],
                              AGE_UP : [NSNumber numberWithInt:[tfAgeTo.text intValue]],
                              AGE_DOWN : [NSNumber numberWithInt:[tfAgeFrom.text intValue]],
                              HEIGHT_UP : [NSNumber numberWithInt:[tfHeightTo.text intValue]],
                              HEIGHT_DOWN : [NSNumber numberWithInt:[tfHeightFrom.text intValue]],
                              FIGURE : figure,
                              HAIR_COLOR : hairColor,
                              ANAL : [NSNumber numberWithInt:(int)[swhAnal isOn] ? 1 : 0],
                              BDSM : [NSNumber numberWithInt:(int)[swhBdsm isOn] ? 1 : 0],
                              BISEXUAL : [NSNumber numberWithInt:(int)[swhBisexual isOn] ? 1 : 0],
                              BLOW_JOB : [NSNumber numberWithInt:(int)[swhBlowJob isOn] ? 1 : 0],
                              DELICATE_SEX : [NSNumber numberWithInt:(int)[swhDelicateSex isOn] ? 1 : 0],
                              DEEP_THROAT : [NSNumber numberWithInt:(int)[swhDeepThroat isOn] ? 1 : 0],
                              DEVOT : [NSNumber numberWithInt:(int)[swhDevot isOn] ? 1 : 0],
                              DIRTY_TALKS : [NSNumber numberWithInt:(int)[swhDirtyTalks isOn] ? 1 : 0],
                              DOMINIANT : [NSNumber numberWithInt:(int)[swhDominant isOn] ? 1 : 0],
                              ESCORT_SERVICE : [NSNumber numberWithInt:(int)[swhEscortService isOn] ? 1 : 0],
                              HAND_JOB : [NSNumber numberWithInt:(int)[swhHandHob isOn] ? 1 : 0],
                              CLASSIC : [NSNumber numberWithInt:(int)[swhClassic isOn] ? 1 : 0],
                              KISSES : [NSNumber numberWithInt:(int)[swhClassic isOn] ? 1 : 0],
                              LEATHER : [NSNumber numberWithInt:(int)[swhLeather isOn] ? 1 : 0],
                              ROLLING_GAMES : [NSNumber numberWithInt:(int)[swhRollingGames isOn] ? 1 : 0],
                              MASTURBATION : [NSNumber numberWithInt:(int)[swhMasturbation isOn] ? 1 : 0],
                              SPANISH : [NSNumber numberWithInt:(int)[swhSpanish isOn] ? 1 : 0],
                              STRIPTEASE : [NSNumber numberWithInt:(int)[swhStriptease isOn] ? 1 : 0],
                              WITHOUT_TABU : [NSNumber numberWithInt:(int)[swhWithoutTabu isOn] ? 1 : 0],
                              TRANSSEXUAL : [NSNumber numberWithInt:(int)[swhTranssexual isOn] ? 1 : 0]
                              };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        [_user._userList removeAllObjects];
        
        NSLog(@"update profile for filter respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSMutableArray *usersDict = [responseObject valueForKey:RES_USERINFO];
            
            for (NSDictionary *dict in usersDict) {
                
                UserEntity *other = [[UserEntity alloc] init];
                
                other._idx = [[dict valueForKey:RES_ID] intValue];
                other._name = [dict valueForKey:RES_NAME];
                other._latitude = [[dict valueForKey:LATITUDE] floatValue];
                other._longitude = [[dict valueForKey:LONGITUDE] floatValue];
                other._address = [dict valueForKey:RES_ADDRESS];
                other._sex = [[dict valueForKey:RES_SEX] intValue];
                other._aboutMe = [dict valueForKey:RES_ABOUTME];
                other._userState = [[dict valueForKey:RES_ISMEMBER] intValue];
                other._phone = [dict valueForKey:RES_PHONE];
                
                
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                
                photoUrl = [dict valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [other._photoUrl addObject:photo_];
                }
                
                other._distance = [[dict valueForKey:RES_DISTANCE] floatValue];
                
                //for filter options
                other._age = [[dict valueForKey:RES_AGE] intValue];
                other._height = [[dict valueForKey:HEIGHT] floatValue];
                other._figure = [dict valueForKey:FIGURE];
                other._hair_color = [dict valueForKey:HAIR_COLOR];
                other._anal = [[dict valueForKey:ANAL] intValue];
                other._bdsm = [[dict valueForKey:BDSM] intValue];
                other._bisexual = [[dict valueForKey:BISEXUAL] intValue];
                other._blow_job = [[dict valueForKey:BLOW_JOB] intValue];
                other._delicate_sex = [[dict valueForKey:DELICATE_SEX] intValue];
                other._deep_throat = [[dict valueForKey:DEEP_THROAT] intValue];
                other._devot = [[dict valueForKey:DEVOT] intValue];
                other._dirty_talks = [[dict valueForKey:DIRTY_TALKS] intValue];
                other._dominant = [[dict valueForKey:DOMINIANT] intValue];
                other._escort_service = [[dict valueForKey:ESCORT_SERVICE] intValue];
                other._hand_job = [[dict valueForKey:HAND_JOB] intValue];
                other._classic = [[dict valueForKey:CLASSIC] intValue];
                other._kisses = [[dict valueForKey:KISSES] intValue];
                other._leather = [[dict valueForKey:LEATHER] intValue];
                other._rolling_games = [[dict valueForKey:ROLLING_GAMES] intValue];
                other._masturbation = [[dict valueForKey:MASTURBATION] intValue];
                other._spanish = [[dict valueForKey:SPANISH] intValue];
                other._stripetease = [[dict valueForKey:STRIPTEASE] intValue];
                other._without_tabu = [[dict valueForKey:WITHOUT_TABU] intValue];
                other._transsexuel = [[dict valueForKey:TRANSSEXUAL] intValue];
                
                if (_user._idx != other._idx) {
                    
                    [_user._userList addObject:other];
                }
            }
            
            if (_user._userList.count > 0) {
                [[JLToast makeText:[NSString stringWithFormat:@"%d %@", (int)_user._userList.count, [APPDELEGATE.locationString objectForKey:@"SEARCHED"] ] duration:2] show];
            } else {
                [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"NO_RESULT"] duration:2] show];
            }
            
        } else if (nResultCode == CODE_INVALIDEMAIL) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else  if(nResultCode == CODE_WRONGPWD) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_PWD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
    
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [self.tableView endEditing:YES];
}

@end

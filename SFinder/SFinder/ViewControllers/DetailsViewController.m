//
//  DetailsViewController.m
//  SFinder
//
//  Created by AOC on 30/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "DetailsViewController.h"

@implementation DetailsViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    NSLog(@"details view did load");
}


- (void) viewWillAppear:(BOOL)animated {    
    
    [super viewWillAppear:animated];
    
    NSLog(@"details view will appear");
}


@end

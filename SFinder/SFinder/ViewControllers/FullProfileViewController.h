//
//  FullProfileViewController.h
//  SFinder
//
//  Created by AOC on 24/01/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"
#import "BaseViewController.h"

@interface FullProfileViewController : UITableViewController
@property (nonatomic, strong) UserEntity *_fullUser;
@end

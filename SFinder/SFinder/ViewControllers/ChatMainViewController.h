//
//  ChatMainViewController.h
//  SFinder
//
//  Created by AOC on 24/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "BaseViewController.h"

@interface ChatMainViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

- (void) reloadData;

@end

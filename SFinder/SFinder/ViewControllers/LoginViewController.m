//
//  LoginViewController.m
//  SFinder
//
//  Created by AOC on 24/01/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"

@interface LoginViewController() {
    
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    __weak IBOutlet UIButton *btnLogin;
    
    UserEntity *_user;
    
}

@end
@implementation LoginViewController


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    btnLogin.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self unregisterNotification];
}

- (BOOL) isValid {
    
    if (tfEmail.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];

        return NO;
    }
    
    if (![CommonUtils isValidEmail:tfEmail.text]) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        
        return NO;
    }
    
    if (tfPassword.text.length < 5) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PASSWORD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];

        return NO;
        
    }
    
    return YES;
    
}

- (IBAction)loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doLogin];
    }
}

- (void) doLogin {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_LOGIN];
    
    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSDictionary * params = @{
                              EMAIL : email,
                              PASSWORD : password
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"login respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSDictionary *dict = [responseObject valueForKey:RES_USERINFO];
            
            _user._idx = [[dict valueForKey:RES_ID] intValue];
            _user._email = email;
            _user._name = [dict valueForKey:RES_NAME];
            _user._age = [[dict valueForKey:RES_AGE] intValue];
            _user._phone = [dict valueForKey:RES_PHONE];
            _user._sex = [[dict valueForKey:RES_SEX] intValue];
            _user._aboutMe = [dict valueForKey:RES_ABOUTME];

            NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
            
            photoUrl = [dict valueForKey:RES_PHOTO];
            
            _user._photoUrl = [[NSMutableArray alloc] init];
            
            for (NSString *photo_ in photoUrl) {
                
                [_user._photoUrl addObject:photo_];
            }
            
            _user._userState = [[dict valueForKey:RES_ISMEMBER] intValue];
            _user._isLoggedIn = YES;
            
            
            [CommonUtils setXmppId:_user._idx];
            
            [CommonUtils saveUserInfo];
            
            //[_user._userList addObject:_user];
            
            // If new user, clear DB
            if([CommonUtils getLastRegisterEmail] == nil || ![[CommonUtils getLastRegisterEmail] isEqualToString:email]) {
                
                [CommonUtils setLastRegisterEmail:email];
                [[DBManager getSharedInstance] clearDB];
            }
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_LOGIN"] duration:2] show];
            
            _from = 1;
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (nResultCode == CODE_INVALIDEMAIL) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
        } else  if(nResultCode == CODE_WRONGPWD) {
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_PWD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];

}

- (IBAction)closeAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];   
    
}



@end

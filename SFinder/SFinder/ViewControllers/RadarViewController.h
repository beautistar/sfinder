//
//  RadarViewController.h
//  SFinder
//
//  Created by Steffen Gruenewald on 21/10/2016.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface RadarViewController : UIViewController <CLLocationManagerDelegate>

@property (nonatomic, retain) CLLocationManager *locManager;

@end
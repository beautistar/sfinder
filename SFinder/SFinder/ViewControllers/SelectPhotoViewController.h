//
//  SelectPhotoViewController.h
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectPhotoViewController : UIViewController {
    
}

@property (nonatomic, strong) NSString *strPhotoPath;

@property (nonatomic, strong) NSMutableArray *arrPhotoPath;

@end

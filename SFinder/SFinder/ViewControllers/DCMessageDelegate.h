//
//  DCMessageDelegate.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#ifndef DCMessageDelegate_h
#define DCMessageDelegate_h

#import <UIKit/UIKit.h>
#import "XmppPacket.h"

@protocol DCMessageDelegate <NSObject>

- (void) newPacketReceived:(XmppPacket *)_revPacket;

@end

@protocol DCRoomMessageDelegate <NSObject>

- (void) newRoomPackedReceived:(XmppPacket *) _revPacket;

@end


@protocol XmppCustomReconnectionDelegate <NSObject>

- (void) xmppConnected;
- (void) xmppDisconnected;

@end

@protocol XmppFriendRequestDelegate <NSObject>

- (void) sendFriendRequest;

@end


#endif /* DCMessageDelegate_h */

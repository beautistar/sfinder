
//  MyProfileViewController.m
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MyProfileViewController.h"
#import "SelectPhotoViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"
@import ActionSheetPicker_3_0;


@interface MyProfileViewController () <UIScrollViewDelegate>{
    
    UserEntity *_user;
  
    __weak IBOutlet UITextView *tvDescription;
    __weak IBOutlet UIView *vContent;
    __weak IBOutlet UITextField *tfPhone;
    __weak IBOutlet UITextField *tfEmail;
//    __weak IBOutlet UITextField *tfAuthCode;
//    __weak IBOutlet UIImageView *imvProfile;
    
    __weak IBOutlet UIView *scrollContentView;
    __weak IBOutlet UIView *scrContentSubView;
    __weak IBOutlet UIView *scrConSubInfoView;
    
    __weak IBOutlet UILabel *lblName;

    __weak IBOutlet UILabel *lblState;

    __weak IBOutlet UIButton *btnRegMember;
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIScrollView *scroll;
    
    __weak IBOutlet UITextField *tfAge;
    __weak IBOutlet UITextField *tfHeight;
    __weak IBOutlet UITextField *tfFigure;
    __weak IBOutlet UITextField *tfHairColor;
   
    int imageCount;
    CGFloat _scrollViewWidth;
    int _pageNumber;
    
    __weak IBOutlet UISwitch *swhAnal;
    __weak IBOutlet UIView *profileContentView;
    
    __weak IBOutlet UISwitch *swhBdsm;
    __weak IBOutlet UISwitch *swhBisexual;
    __weak IBOutlet UISwitch *swhBlowJob;
    __weak IBOutlet UISwitch *swhDelicateSex;
    __weak IBOutlet UISwitch *swhDeepThroat;
    __weak IBOutlet UISwitch *swhDevot;
    __weak IBOutlet UISwitch *swhDirtyTalks;
    __weak IBOutlet UISwitch *swhDominant;
    __weak IBOutlet UISwitch *swhEscortService;
    __weak IBOutlet UISwitch *swhClassic;
    __weak IBOutlet UISwitch *swhKisses;
    __weak IBOutlet UISwitch *swhHandJob;
    __weak IBOutlet UISwitch *swhLeather;
    __weak IBOutlet UISwitch *swhRollingGames;
    __weak IBOutlet UISwitch *swhSpanish;
    __weak IBOutlet UISwitch *swhMasturbation;
    __weak IBOutlet UISwitch *swhStriptease;
    __weak IBOutlet UISwitch *swhTranssexual;
    __weak IBOutlet UISwitch *swhWithoutTabu;
}
@end



@implementation MyProfileViewController

@synthesize arrPhotoPath;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _from = 0;
    
    arrPhotoPath = [NSMutableArray array];
    
    [self initKeyboard];
    
}

- (void) initKeyboard {
    
    //keybiard Done button add
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    [numberToolbar sizeToFit];
    tfAge.inputAccessoryView = numberToolbar;
    tfHeight.inputAccessoryView = numberToolbar;

}

-(void)doneWithNumberPad{
    
    [tfAge resignFirstResponder];
    [tfHeight resignFirstResponder];
}

- (IBAction)hairColorAction:(id)sender {
    
     [self.view endEditing:YES];
    
    NSArray *colors = [NSArray arrayWithObjects:[APPDELEGATE.locationString objectForKey:@"BLACK"], [APPDELEGATE.locationString objectForKey:@"BLOND"], [APPDELEGATE.locationString objectForKey:@"BROWN"], [APPDELEGATE.locationString objectForKey:@"RED"], [APPDELEGATE.locationString objectForKey:@"CHOOSEN"], nil];
    
    [ActionSheetStringPicker showPickerWithTitle:[APPDELEGATE.locationString objectForKey:@"SEL_HAIRCOLOR"]
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           tfHairColor.text = selectedValue;
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
}

- (IBAction)figureAction:(id)sender {
    
     [self.view endEditing:YES];
    
    NSArray *figures = [NSArray arrayWithObjects:[APPDELEGATE.locationString objectForKey:@"SLENDER"], [APPDELEGATE.locationString objectForKey:@"SPORTY"], [APPDELEGATE.locationString objectForKey:@"CHUBBY"], [APPDELEGATE.locationString objectForKey:@"NORMAL"], [APPDELEGATE.locationString objectForKey:@"PREF"], nil];
    
    [ActionSheetStringPicker showPickerWithTitle:[APPDELEGATE.locationString objectForKey:@"SEL_FIGURE"]
                                            rows:figures
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           tfFigure.text = selectedValue;
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _user = APPDELEGATE.Me;
    
    [CommonUtils loadUserInfo];
    
    [self initView];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:NO];
    
    tvDescription.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvDescription.layer.borderWidth = 0.5f;
    
    if (_user._userState == STATE_MEMBER) {
        
        [btnRegMember setHidden:YES];
        [vContent setHidden:NO];
        
    } else {
        
        [btnRegMember setHidden:NO];
        [vContent setHidden:YES];
    }

    [self setProfilePhoto];

}

- (void) setProfilePhoto {
    
    if (_from == 0)
    imageCount = (int)_user._photoUrl.count;
    else imageCount = (int) arrPhotoPath.count;
    // Scroll View
    
    scroll.backgroundColor=[UIColor clearColor];
    scroll.delegate=self;
    scroll.pagingEnabled=YES;
    scroll.alwaysBounceVertical = NO;
    scroll.bounces = NO;
    
    [scroll setContentSize:CGSizeMake(self.view.frame.size.width*imageCount, self.view.frame.size.height*0.38)];
    
    // page control
    
    pageControl.backgroundColor=[UIColor clearColor];
    pageControl.numberOfPages=imageCount;
    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    
    if (_from != FROM_TAKEPHOTO) {
        
        if (imageCount == 0 ) {
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, scroll.frame.size.height)];
            
            image.image = [UIImage imageNamed:@"profile"];

            [scroll addSubview:image];
        } else {
            
            for(int i=1;i<=imageCount;i++) {
                
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, scroll.frame.size.height)];
                [image setImageWithURL:[NSURL URLWithString:_user._photoUrl[i-1]] placeholderImage:[UIImage imageNamed:@"profile"]];
                [scroll addSubview:image];
                
                x+=self.view.frame.size.width;
            }
        }
    } else {
        
        if (imageCount == 0 ) {
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, scroll.frame.size.height)];
            [image setImageWithURL:[NSURL URLWithString:_user.getProfile] placeholderImage:[UIImage imageNamed:@"profile"]];
            [scroll addSubview:image];
        
        } else {
            
            for(int i=1;i<=imageCount;i++) {
                    
                UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, scroll.frame.size.height)];
                [image setImage:[UIImage imageWithContentsOfFile:arrPhotoPath[i-1]]];
                [scroll addSubview:image];
                x+=self.view.frame.size.width;
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    _scrollViewWidth = _scrollView.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    _pageNumber = floor((_scrollView.contentOffset.x - _scrollViewWidth/50) / _scrollViewWidth) +1;
    pageControl.currentPage=_pageNumber;
}

- (void)pageChanged {
    
    int pageNumber = (int)pageControl.currentPage;
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [scroll scrollRectToVisible:frame animated:YES];
}

- (void) autoScroll {
    
    pageControl.currentPage=_pageNumber++;
    
    if (_pageNumber >= imageCount) {
        _pageNumber = 0;
    }
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*_pageNumber;
    frame.origin.y=0;
    
    if (_pageNumber == 0) {
        [scroll scrollRectToVisible:frame animated:NO];
    } else {
        [scroll scrollRectToVisible:frame animated:YES];
    }
}

- (void) initView {
    
//    if (_from != FROM_TAKEPHOTO) {
//        [imvProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl[0]] placeholderImage:[UIImage imageNamed:@"profile"]];
//    }
    
//    if (_user._userState == STATE_REGISTERED) {
//        
//        vContent.hidden = YES;
//    } else {
//        vContent.hidden = NO;
//    }
    
    tfPhone.text = _user._phone;
    tfEmail.text = _user._email;
    if (_user._aboutMe.length == 0) {
        tvDescription.placeholder = @"About me";
    } else {
        tvDescription.text = _user._aboutMe;
    }
    
    lblName.text = _user._name;
    
    switch (_user._userState) {
        case STATE_REGISTERED:
            lblState.text = [APPDELEGATE.locationString objectForKey:@"Q_REGISTRAION"] ;
            break;
        case STATE_MEMBER :
            lblState.text = [APPDELEGATE.locationString objectForKey:@"IAMAMEMEBER"];
            break;
        default:
            lblState.text = @"I'm a Admin";
            break;
    }

    // for filter options
//    tfAge.text = [NSString stringWithFormat:@"%d", _user._age];
//    tfHeight.text = [NSString stringWithFormat:@"%.1f", _user._height];
    tfFigure.text = _user._figure;
    tfHairColor.text = _user._hair_color;
    _user._anal == 1 ? [swhAnal setOn:YES] : [swhAnal setOn:NO];
    _user._bdsm == 1 ? [swhBdsm setOn:YES] : [swhBdsm setOn:NO];
    _user._bisexual == 1 ? [swhBisexual setOn:YES] : [swhBdsm setOn:NO];
    _user._blow_job == 1 ? [swhBlowJob setOn:YES] : [swhBlowJob setOn:NO];
    _user._delicate_sex == 1 ? [swhDelicateSex setOn:YES] : [swhDelicateSex setOn:NO];
    _user._deep_throat == 1 ? [swhDeepThroat setOn:YES] : [swhDeepThroat setOn:NO];
    _user._devot == 1 ? [swhDevot setOn:YES] : [swhDevot setOn:NO];
    _user._dirty_talks == 1 ? [swhDirtyTalks setOn:YES] : [swhDirtyTalks setOn:NO];
    _user._dominant == 1 ? [swhDominant setOn:YES] : [swhDominant setOn:NO];
    _user._escort_service == 1 ? [swhEscortService setOn:YES] : [swhEscortService setOn:NO];
    _user._hand_job == 1 ? [swhHandJob setOn:YES] : [swhHandJob setOn:NO];
    _user._delicate_sex == 1 ? [swhDelicateSex setOn:YES] : [swhDelicateSex setOn:NO];
    _user._deep_throat == 1 ? [swhDeepThroat setOn:YES] : [swhDeepThroat setOn:NO];
    _user._devot == 1 ? [swhDevot setOn:YES] : [swhDevot setOn:NO];
    _user._dirty_talks == 1 ? [swhDirtyTalks setOn:YES] : [swhDirtyTalks setOn:NO];
    _user._dominant == 1 ? [swhDominant setOn:YES] : [swhDominant setOn:NO];
    _user._escort_service == 1 ? [swhEscortService setOn:YES] : [swhEscortService setOn:NO];
    _user._hand_job == 1 ? [swhHandJob setOn:YES] : [swhHandJob setOn:NO];
    _user._classic == 1 ? [swhClassic setOn:YES] : [swhClassic setOn:NO];
    _user._kisses == 1 ? [swhClassic setOn:YES] : [swhClassic setOn:NO];
    _user._leather == 1 ? [swhLeather setOn:YES] : [swhLeather setOn:NO];
    _user._rolling_games == 1 ? [swhRollingGames setOn:YES] : [swhRollingGames setOn:NO];
    _user._masturbation == 1 ? [swhMasturbation setOn:YES] : [swhMasturbation setOn:NO];
    _user._spanish == 1 ? [swhSpanish setOn:YES] : [swhSpanish setOn:NO];
    _user._stripetease == 1 ? [swhStriptease setOn:YES] : [swhStriptease setOn:NO];
    _user._without_tabu == 1 ? [swhWithoutTabu setOn:YES] : [swhWithoutTabu setOn:NO];
    _user._transsexuel == 1 ? [swhTranssexual setOn:YES] : [swhTranssexual setOn:NO];
    
}

- (IBAction)cameraAction:(id)sender {
    
    SelectPhotoViewController *destVC = (SelectPhotoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SelectPhotoViewController"];
    
    [self presentViewController:destVC animated:YES completion:nil];
}

- (IBAction)sendCodeAction:(id)sender {
    
    if ([self isValidForVerify]) {
        
        [self getAuthCodeForUpdate];
    }
}

- (IBAction)saveAction:(id)sender {
    
    if ([self isValid]) {
        
        if (arrPhotoPath.count != 0) {
        
            [self updateProfile];
        } else {
            [self updateProfileForFilter];
        }
    }
}

- (BOOL) isValid {
    
    if (tfPhone.text.length == 0) {
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PHONE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfEmail.text.length == 0) {
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (![CommonUtils isValidEmail:tfEmail.text]) {
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (_user._userState == STATE_MEMBER) {
    
        if (tfAge.text.length == 0) {
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_AGE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            return NO;
        }
        
        if (tfHeight.text.length == 0) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_HEIGHT"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
            return NO;
        }
        
        if (tfFigure.text.length == 0) {
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_FIGURE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
            return NO;
        }
        
        if (tfHairColor.text.length == 0) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_HAIRCOLOR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
            return NO;
        }
    }
    
//    if (photoPath.length == 0) {
//        
//        [self showAlertDialog:ALERT_TITLE message:INPUT_PHOTO positive:ALERT_OK      negative:nil];
//        return NO;
//        
//    }
    
    return YES;
}

- (BOOL) isValidForVerify {
    
    if (tfEmail.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        
        return NO;
        
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        
        return NO;
    }
    
    return YES;
}

- (IBAction) prepareForUnwindToMyProfile:(UIStoryboardSegue *)segue {   
   
    
    SelectPhotoViewController *selectPhotoVC = (SelectPhotoViewController *) segue.sourceViewController;
    
    photoPath = selectPhotoVC.strPhotoPath;
    
//    [imvProfile setImage:[UIImage imageWithContentsOfFile:selectPhotoVC.strPhotoPath]];
    
    if ([segue.identifier isEqualToString:@"segueunwind2myprofile"]) {
        
        SelectPhotoViewController *sourceVC = (SelectPhotoViewController *)[segue sourceViewController];
        self.arrPhotoPath = sourceVC.arrPhotoPath;
        
//        NSLog(@"profile image count: %ld", self.arrPhotoPath.count);
        
//        for (NSString * _photoPath in self.arrPhotoPath) {
//            NSLog(@"profile image path: %@\n", _photoPath);
//        }
        
        [self setProfilePhoto];
        
        //_from = 0;
    }
}

- (void) getAuthCodeForUpdate {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETCODEFORUPDATE];
    
    NSDictionary * params = @{
                              EMAIL : tfEmail.text
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"authcode respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"VALID_AUTHCODE"] duration:2] show];
            
        } else {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_EMAILADDRESS"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        
    }];
}

- (IBAction)switchAction:(id)sender {
    
}

- (void) updateProfile {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATEPROFILE];
    
    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *phone = [tfPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSDictionary * params = @{RES_ID : [NSNumber numberWithInteger:_user._idx],
                              EMAIL : email,
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
//                              AUTHCODE : tfAuthCode.text,
                              RES_PHONE : phone,
                              RES_ADDRESS : _user._address,
                              RES_ABOUTME : [tvDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],                              
                              RES_AGE : [tfAge.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
                              HEIGHT : [tfHeight.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
                              FIGURE : [tfFigure.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
                              HAIR_COLOR : [tfHairColor.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
                              ANAL : [NSNumber numberWithInteger:(int)[swhAnal isOn] ? 1 : 0],
                              BDSM : [NSNumber numberWithInteger:(int)[swhBdsm isOn] ? 1 : 0],
                              BISEXUAL : [NSNumber numberWithInteger:(int)[swhBisexual isOn] ? 1 : 0],
                              BLOW_JOB : [NSNumber numberWithInteger:(int)[swhBlowJob isOn] ? 1 : 0],
                              DELICATE_SEX : [NSNumber numberWithInteger:(int)[swhDelicateSex isOn] ? 1 : 0],
                              DEEP_THROAT : [NSNumber numberWithInteger:(int)[swhDeepThroat isOn] ? 1 : 0],
                              DEVOT : [NSNumber numberWithInteger:(int)[swhDevot isOn] ? 1 : 0],
                              DIRTY_TALKS : [NSNumber numberWithInteger:(int)[swhDirtyTalks isOn] ? 1 : 0],
                              DOMINIANT : [NSNumber numberWithInteger:(int)[swhDominant isOn] ? 1 : 0],
                              ESCORT_SERVICE : [NSNumber numberWithInteger:(int)[swhEscortService isOn] ? 1 : 0],
                              HAND_JOB : [NSNumber numberWithInteger:(int)[swhHandJob isOn] ? 1 : 0],
                              CLASSIC : [NSNumber numberWithInteger:(int)[swhClassic isOn] ? 1 : 0],
                              KISSES : [NSNumber numberWithInteger:(int)[swhClassic isOn] ? 1 : 0],
                              LEATHER : [NSNumber numberWithInteger:(int)[swhLeather isOn] ? 1 : 0],
                              ROLLING_GAMES : [NSNumber numberWithInteger:(int)[swhRollingGames isOn] ? 1 : 0],
                              MASTURBATION : [NSNumber numberWithInteger:(int)[swhMasturbation isOn] ? 1 : 0],
                              SPANISH : [NSNumber numberWithInteger:(int)[swhSpanish isOn] ? 1 : 0],
                              STRIPTEASE : [NSNumber numberWithInteger:(int)[swhStriptease isOn] ? 1 : 0],
                              WITHOUT_TABU : [NSNumber numberWithInteger:(int)[swhWithoutTabu isOn] ? 1 : 0],
                              TRANSSEXUAL : [NSNumber numberWithInteger:(int)[swhTranssexual isOn] ? 1 : 0]                              
                              };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        int i = 0;
        if (arrPhotoPath.count != 0) {
            for (NSString *photoPath in arrPhotoPath) {
                
                [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:[NSString stringWithFormat:@"%@%d%@", @"file[", i, @"]"] fileName:@"filename.png" mimeType:@"image/png" error:nil];
                i++;
            }
        } 
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [self hideLoadingView];
                        
                          [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"UPDATE_FAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
                      }
                      
                      else {
                          
                          [self hideLoadingView];
                          
                          NSLog(@"register respond : %@", responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              //if (_user._photoUrl.count == 0) {
                                  _user._photoUrl = [NSMutableArray array];
                              //}
                              NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                              
                              photoUrl = [responseObject valueForKey:RES_FILEURL];
                              
                              for (NSString *photo_ in photoUrl) {
                                  
                                  [_user._photoUrl addObject:photo_];
                              }
                              
                              _user._email = email;
                              _user._phone = phone;
                              _user._aboutMe = tvDescription.text;
                              
                              //for filter
                              _user._age = [tfAge.text intValue];
                              _user._height = [tfHeight.text floatValue];
                              _user._figure = tfFigure.text;
                              _user._hair_color = tfHairColor.text;
                              _user._anal = [swhAnal isOn] ? 1 : 0;
                              _user._bdsm = [swhBdsm isOn]?1:0;
                              _user._bisexual = [swhBisexual isOn]?1:0;
                              _user._blow_job = [swhBlowJob isOn]?1:0;
                              _user._delicate_sex = [swhDelicateSex isOn]?1:0;
                              _user._deep_throat = [swhDeepThroat isOn]?1:0;
                              _user._devot = [swhDevot isOn]?1:0;
                              _user._dirty_talks = [swhDirtyTalks isOn]?1:0;
                              _user._dominant = [swhDominant isOn]?1:0;
                              _user._escort_service = [swhEscortService isOn]?1:0;
                              _user._hand_job = [swhHandJob isOn]?1:0;
                              _user._classic = [swhClassic isOn]?1:0;
                              _user._kisses = [swhKisses isOn]?1:0;
                              _user._leather = [swhLeather isOn]?1:0;
                              _user._rolling_games = [swhRollingGames isOn]?1:0;
                              _user._masturbation = [swhMasturbation isOn]?1:0;
                              _user._spanish = [swhSpanish isOn]?1:0;
                              _user._stripetease = [swhStriptease isOn]?1:0;
                              _user._without_tabu = [swhWithoutTabu isOn]?1:0;
                              _user._transsexuel = [swhTranssexual isOn]?1:0;
                              
                              [CommonUtils saveUserInfo];
                              
                              _from = 0;
                              
                              [arrPhotoPath removeAllObjects];
                              
                              [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_UPDATE"] duration:2] show];
                              
                          } else if (nResultCode == CODE_EXISTNAME) {
                              
                              [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
                          } else if (nResultCode == CODE_INVALIDEMAIL) {
                              
                              [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
                          } else  if(nResultCode == CODE_WRONGAUTHCODE) {
                              
                              [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
                          } else if (nResultCode == CODE_INVALID_PHOTO) {

                              [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"UPLOAD_FAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
                              
                          }
                      }
                 }];
    
     [uploadTask resume];
}

- (void) updateProfileForFilter {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATEPROFILEFORFILTER];
    
    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *phone = tfPhone.text;
    
    NSDictionary * params = @{RES_ID : [NSNumber numberWithInteger:_user._idx],
                              EMAIL : email,
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
                              RES_PHONE : phone,
                              RES_ADDRESS : _user._address,
                              RES_ABOUTME : tvDescription.text,
                              
                              RES_AGE : tfAge.text,
                              HEIGHT : tfHeight.text,
                              FIGURE : tfFigure.text,
                              HAIR_COLOR : tfHairColor.text,
                              ANAL : [NSNumber numberWithInteger:(int)[swhAnal isOn] ? 1 : 0],
                              BDSM : [NSNumber numberWithInteger:(int)[swhBdsm isOn] ? 1 : 0],
                              BISEXUAL : [NSNumber numberWithInteger:(int)[swhBisexual isOn] ? 1 : 0],
                              BLOW_JOB : [NSNumber numberWithInteger:(int)[swhBlowJob isOn] ? 1 : 0],
                              DELICATE_SEX : [NSNumber numberWithInteger:(int)[swhDelicateSex isOn] ? 1 : 0],
                              DEEP_THROAT : [NSNumber numberWithInteger:(int)[swhDeepThroat isOn] ? 1 : 0],
                              DEVOT : [NSNumber numberWithInteger:(int)[swhDevot isOn] ? 1 : 0],
                              DIRTY_TALKS : [NSNumber numberWithInteger:(int)[swhDirtyTalks isOn] ? 1 : 0],
                              DOMINIANT : [NSNumber numberWithInteger:(int)[swhDominant isOn] ? 1 : 0],
                              ESCORT_SERVICE : [NSNumber numberWithInteger:(int)[swhEscortService isOn] ? 1 : 0],
                              HAND_JOB : [NSNumber numberWithInteger:(int)[swhHandJob isOn] ? 1 : 0],
                              CLASSIC : [NSNumber numberWithInteger:(int)[swhClassic isOn] ? 1 : 0],
                              KISSES : [NSNumber numberWithInteger:(int)[swhClassic isOn] ? 1 : 0],
                              LEATHER : [NSNumber numberWithInteger:(int)[swhLeather isOn] ? 1 : 0],
                              ROLLING_GAMES : [NSNumber numberWithInteger:(int)[swhRollingGames isOn] ? 1 : 0],
                              MASTURBATION : [NSNumber numberWithInteger:(int)[swhMasturbation isOn] ? 1 : 0],
                              SPANISH : [NSNumber numberWithInteger:(int)[swhSpanish isOn] ? 1 : 0],
                              STRIPTEASE : [NSNumber numberWithInteger:(int)[swhStriptease isOn] ? 1 : 0],
                              WITHOUT_TABU : [NSNumber numberWithInteger:(int)[swhWithoutTabu isOn] ? 1 : 0],
                              TRANSSEXUAL : [NSNumber numberWithInteger:(int)[swhTranssexual isOn] ? 1 : 0]
                              };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"update profile for filter respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            _user._email = email;
            _user._phone = phone;
            _user._aboutMe = tvDescription.text;
            
            //for filter
            _user._age = [tfAge.text intValue];
            _user._height = [tfHeight.text floatValue];
            _user._figure = tfFigure.text;
            _user._hair_color = tfHairColor.text;
            _user._anal = [swhAnal isOn] ? 1 : 0;
            _user._bdsm = [swhBdsm isOn]?1:0;
            _user._bisexual = [swhBisexual isOn]?1:0;
            _user._blow_job = [swhBlowJob isOn]?1:0;
            _user._delicate_sex = [swhDelicateSex isOn]?1:0;
            _user._deep_throat = [swhDeepThroat isOn]?1:0;
            _user._devot = [swhDevot isOn]?1:0;
            _user._dirty_talks = [swhDirtyTalks isOn]?1:0;
            _user._dominant = [swhDominant isOn]?1:0;
            _user._escort_service = [swhEscortService isOn]?1:0;
            _user._hand_job = [swhHandJob isOn]?1:0;
            _user._classic = [swhClassic isOn]?1:0;
            _user._kisses = [swhKisses isOn]?1:0;
            _user._leather = [swhLeather isOn]?1:0;
            _user._rolling_games = [swhRollingGames isOn]?1:0;
            _user._masturbation = [swhMasturbation isOn]?1:0;
            _user._spanish = [swhSpanish isOn]?1:0;
            _user._stripetease = [swhStriptease isOn]?1:0;
            _user._without_tabu = [swhWithoutTabu isOn]?1:0;
            _user._transsexuel = [swhTranssexual isOn]?1:0;
            
            [CommonUtils saveUserInfo];
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_UPDATE"] duration:2] show];
            
            _from = 1;
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (nResultCode == CODE_INVALIDEMAIL) {

            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else  if(nResultCode == CODE_WRONGPWD) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_PWD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
       
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
    
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [vContent endEditing:YES];
    [scrConSubInfoView endEditing:YES];
    [scrContentSubView endEditing:YES];
    [scrollContentView endEditing:YES];
}

@end

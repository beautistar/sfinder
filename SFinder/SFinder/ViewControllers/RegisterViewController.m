//
//  RegisterViewController.m
//  SFinder
//
//  Created by AOC on 07/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "RegisterViewController.h"
#import "CommonUtils.h"

#import "DBManager.h"

@interface RegisterViewController () {
    
    __weak IBOutlet UIButton *btnMakePayment;
    __weak IBOutlet UIButton *btnRegister;
    __weak IBOutlet UIButton *btnAGB;
    __weak IBOutlet UIButton *btnSendCode;
    __weak IBOutlet UIButton *btnLogin;
    __weak IBOutlet UIButton *btnLogout;
    
    __weak IBOutlet UITextField *tfPhone;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfUserName;
    __weak IBOutlet UITextField *tfAuthCode;
    __weak IBOutlet UITextField *tfPassword;
    
    UserEntity *_user;
    
    BOOL isEmailVerified;
}

@end

@implementation RegisterViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    isEmailVerified = NO;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    btnMakePayment.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnRegister.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnAGB.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnSendCode.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnLogin.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnLogout.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    [self unregisterNotification];
    
    if (_from == 1) { // if it appear after login, then disappear
        
        _from = 0;
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if (_user._isLoggedIn || _user._userState >= STATE_REGISTERED) {
        [btnLogout setHidden:NO];//show
    } else {
        [btnLogout setHidden:YES];
    }
}

- (IBAction)closeAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (IBAction)sendCodeAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self isValidForVerify]) {
        
        [self getAuthCode];
    }
}

- (IBAction)registerAction:(id)sender {
    

    if ([self isValid]) {
    
        [self doRegister];
    }
}

- (IBAction)regsiterMemberAction:(id)sender {
    
    if ([self isValid]) {
        
        [self registerMember];
    }
}

- (BOOL) isValid {
    
    if (_user._latitude == 0.0 || _user._longitude == 0 || _user._address.length == 0) {
        
        [[JLToast makeText:@"Searching your address"] show];
        
        return NO;
    }
    
    if (tfPhone.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PHONE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfEmail.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfPassword.text.length < 5) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PWD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
        
    }
    
    if (tfUserName.text.length == 0) {
       
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfAuthCode.text.length == 0) {

        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (![CommonUtils isValidEmail:tfEmail.text]) {

        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    return YES;
}

- (BOOL) isValidForVerify {
    
    if (tfEmail.text.length == 0) {
      
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {

        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    return YES;
}

- (void) getAuthCode {

    [self showLoadingView];

    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETAUTHCODE];

    NSDictionary * params = @{
                              EMAIL : tfEmail.text
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request

    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        [self hideLoadingView];

        [_user._userList removeAllObjects];

        NSLog(@"authcode respond : %@", responseObject);

        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];

        if(nResultCode == CODE_SUCCESS) {

            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"VALID_AUTHCODE"] duration:2] show];
            
        } else {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_EMAILADDRESS"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
}

- (void) doRegister {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_REGISTER];

    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name = tfUserName.text;
    NSString *phone = tfPhone.text;
    
    NSDictionary * params = @{
                              EMAIL : email,
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
                              RES_NAME : name,
                              AUTHCODE : [tfAuthCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                              RES_PHONE : phone,
                              PASSWORD : [tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                              RES_ADDRESS : _user._address
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"register respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_ID] intValue];
            _user._email = email;
            _user._name = name;
            _user._phone = phone;
            _user._userState = STATE_REGISTERED;
            _user._isLoggedIn = YES;
            
            [CommonUtils setXmppId:_user._idx];
            
            [CommonUtils saveUserInfo];
            
            // If new user, clear DB
            if([CommonUtils getLastRegisterEmail] == nil || ![[CommonUtils getLastRegisterEmail] isEqualToString:email]) {
                
                [CommonUtils setLastRegisterEmail:email];
                [[DBManager getSharedInstance] clearDB];
            }
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_REGISER"] duration:2] show];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (nResultCode == CODE_EXISTNAME) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else if (nResultCode == CODE_INVALIDEMAIL) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else  if(nResultCode == CODE_WRONGAUTHCODE) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];    
}

- (void) registerMember {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_REGISTERMEMBER];
    
    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name = tfUserName.text;
    NSString *phone = tfPhone.text;
    
    NSDictionary * params = @{
                              EMAIL : email,
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
                              RES_NAME : name,
                              AUTHCODE : tfAuthCode.text,
                              RES_PHONE : phone,
                              RES_ADDRESS : _user._address
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"register respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_ID] intValue];
            _user._email = email;
            _user._name = name;
            _user._phone = phone;
            _user._userState = STATE_MEMBER;
            
            [CommonUtils saveUserInfo];
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_MEMBERREGISTER"] duration:2] show];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (nResultCode == CODE_EXISTNAME) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else if (nResultCode == CODE_INVALIDEMAIL) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else  if(nResultCode == CODE_WRONGAUTHCODE) {
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
    
}

- (IBAction)logoutAction:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //    _user = [[UserEntity alloc] init];
    _user._idx = 0;
    _user._email = @"";
    _user._phone = @"";
    _user._name = @"";
    _user._aboutMe = @"";
    _user._userState = 0;
    [CommonUtils saveUserInfo];
    _user._isLoggedIn = NO;
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:(UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"IntroNav"]];
}

@end

/*
 
 The MIT License (MIT)
 
 Copyright (c) 2015 ABM Adnan
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

#import "RadarViewController.h"
#import "RegisterViewController.h"
#import "MapViewController.h"

#import "Arcs.h"
#import "Radar.h"
#import "Dot.h"

#import "CommonUtils.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

@interface RadarViewController () <UITabBarControllerDelegate, UIGestureRecognizerDelegate> {
    __weak IBOutlet UIView *radarViewHolder;
    __weak IBOutlet UIView *radarLine;
    __weak IBOutlet UISlider *distanceSlider;
    __weak IBOutlet UILabel *lblInfo;
    Arcs *arcsView;
    Radar *radarView;
    float currentDeviceBearing;
    NSMutableArray *dots;
    NSMutableArray *nearbyUsers;
    NSTimer *detectCollisionTimer;
    CGFloat radarHolderWidth;
    
    UserEntity * _user;
    int nearbyCount;
    
}

@end

@implementation RadarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITabBarController *tabVC = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    [tabVC setDelegate:self];
    
    UIPinchGestureRecognizer *_pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    _pinch.delegate = self;
    [self.view addGestureRecognizer:_pinch];
    
    
    _user = APPDELEGATE.Me;
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGFloat screenWidth = screenRect.size.width;
        
        radarHolderWidth = screenWidth + 80;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"We are new and we are growing. Please help us and tell your friends about SFinder." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
}


- (void) updateRadar
{
    [arcsView removeFromSuperview];
    [radarView removeFromSuperview];

    [self setRadar];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.hidesBottomBarWhenPushed = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [self.tabBarController.tabBar setHidden:NO];

    [self updateRadar];
    
}

-(void)handlePinchGesture:(UIGestureRecognizer *)recognizer
{
    //    static CGRect initialBounds;
    
    //    if (recognizer.state == UIGestureRecognizerStateBegan)
    //    {
    //        initialBounds = self.view.bounds;
    //    }
    CGFloat factor = [(UIPinchGestureRecognizer *)recognizer scale];
    
    NSLog(@"pinch factor : %f", factor);
    //    CGAffineTransform zoom = CGAffineTransformScale(CGAffineTransformIdentity, factor, factor);
    //    self.view.bounds = CGRectApplyAffineTransform(initialBounds, zoom);
    
//    decrease
//    distanceSlider.value += - (distanceSlider.maximumValue - distanceSlider.minimumValue) * 0.1;
//    [self sliderValueChange: distanceSlider];
//    
////    increase
//    distanceSlider.value += (distanceSlider.maximumValue - distanceSlider.minimumValue) * 0.1;
//    [self sliderValueChange: distanceSlider];
    
    if (factor > 1) {
    distanceSlider.value += - (distanceSlider.maximumValue - distanceSlider.minimumValue) * factor * 0.005;
    [self sliderValueChange: distanceSlider];
    } else {
    //    increase
    distanceSlider.value += (distanceSlider.maximumValue - distanceSlider.minimumValue) * factor * 0.005;
    [self sliderValueChange: distanceSlider];
    }
}

#pragma mark -
#pragma mark UIGestureDelegate
#pragma mark -
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (![gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] && ![otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}


- (void) setRadar{
    // Do any additional setup after loading the view,  from a nib.
    dots = [[NSMutableArray alloc] init];
    nearbyUsers = [[NSMutableArray alloc] init];
    
    arcsView = [[Arcs alloc] initWithFrame:CGRectMake(0, 0, radarHolderWidth, radarHolderWidth)];
    
    // NOTE: Since our gradient layer is built as an image,
    // we need to scale it to match the display of the device.
    arcsView.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    
    radarViewHolder.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    
    [radarViewHolder addSubview:arcsView];
    
    //arcsView.backgroundColor = [UIColor blueColor];
    
    // add tap gesture recognizer to arcs view to capture tap on dots (user profiles) and enlarge the selected dots with a white border
    arcsView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDotTapped:)];
    [arcsView addGestureRecognizer:tapGestureRecognizer];
    
    radarView = [[Radar alloc] initWithFrame:CGRectMake(3, 3, radarHolderWidth-6, radarHolderWidth-6)];
    
    
    //NSLog(@"width - %lf , height - %lf " , radarHolderWidth, radarHolderWidth);
    radarView.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    
    radarView.alpha = 0.68;
    
    //radarView.backgroundColor = [UIColor whiteColor];
    
    [radarViewHolder addSubview:radarView];
    
    // start spinning the radar forever
    [self spinRadar];
    
    // start heading event to rotate the arcs along with device rotation
    currentDeviceBearing = 0;
    [self startHeadingEvent];    
    [self loadUsers];
}


#pragma mark - Reload Radar

-(void) removePreviousDots {
    for (Dot *dot in dots) {
        [dot removeFromSuperview];
    }
    dots = [NSMutableArray array];
    
    // reset slider
    distanceSlider.minimumValue = 0;
    distanceSlider.maximumValue = 0;
    distanceSlider.value = 0;
}


-(void)renderUsersOnRadar:(NSMutableArray *)users{
    
    CLLocationCoordinate2D myLoc = { _user._latitude, _user._longitude };
    
    // the last user in the nearbyUsers list is the farthest
    float maxDistance = [[[users lastObject] valueForKey:@"distance"] floatValue];
    float minDistance = [[[users firstObject] valueForKey:@"distance"] floatValue];
    
    distanceSlider.minimumValue = minDistance;
    distanceSlider.maximumValue = maxDistance;
    distanceSlider.value = minDistance + (maxDistance - minDistance)/2;
    
    int i = 0;
    // add users dots
    
    //UserEntity *user_ = [[UserEntity alloc] init];
    for (NSDictionary *user in users) {
        
        i++;
        
        
        // male > blue, female > green
//        if ([[user valueForKey:@"gender"] isEqualToString:@"female"]) {
//            // pink
//            dot.color = [UIColor colorWithRed:234.0/255.0 green:69.0/255.0 blue:130.0/255.0 alpha:1.0];
//        } else {
//            // cyan
//            dot.color = [UIColor colorWithRed:39.0/255.0 green:185.0/255.0 blue:173.0/255.0 alpha:1.0];
//        }
//        
//        if ([[user valueForKey:@"gender"] isEqualToString:@"female"]) {
//            // pink
//            dot.color = [UIColor colorWithRed:234.0/255.0 green:69.0/255.0 blue:130.0/255.0 alpha:1.0];
//        } else {
//            // cyan
//            dot.color = [UIColor colorWithRed:39.0/255.0 green:185.0/255.0 blue:173.0/255.0 alpha:1.0];
//        }
        
        UserEntity *user_ = [[UserEntity alloc] init];
        user_ = (UserEntity *)_user._userList[i-1];
        
        Dot *dot;
        
        if (user_._userState < STATE_MEMBER) continue;
        
        if (_user._userState == STATE_UNREGISTERED ) {
            
            dot = [[Dot alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            
            dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
            
            dot.color = [UIColor colorWithRed:234.0/255.0 green:69.0/255.0 blue:130.0/255.0 alpha:1.0];
            
        } else /*if (_user._userState == STATE_REGISTERED) */{
            
            dot = [[Dot alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            
            dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
            
            [dot setImageWithUrl:user_.getProfile] ;
            
            if (_user._idx == user_._idx) {
                
                dot.hidden = YES;
            }
            
            //dot.color = [UIColor colorWithRed:234.0/255.0 green:69.0/255.0 blue:130.0/255.0 alpha:1.0];
        }
        
        CLLocationCoordinate2D userLoc = { [[user valueForKey:@"lat"] floatValue], [[user valueForKey:@"lng"] floatValue] };
        
        float bearing = [self getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        dot.bearing = [NSNumber numberWithFloat:bearing];
        
        float d = [[user valueForKey:@"distance"] floatValue];
        float distance = MAX((radarHolderWidth/ 2 - 8) / 4, d * (radarHolderWidth/ 2 - 8) / maxDistance); //35// 140 = radius of the radar circle, so the farthest user will be on the perimiter of radar circle
        
        dot.distance = [NSNumber numberWithFloat:distance]; // relative distance
        
        dot.userDistance = [NSNumber numberWithFloat:d];
        dot.userProfile = user;
        dot.zoomEnabled = NO;
        dot.userInteractionEnabled = NO;

        
        [self rotateDot:dot fromBearing:0 toBearing:bearing atDistance:distance];
        
        [arcsView addSubview:dot];
        
        [dots addObject:dot];        
    }
    
    [self sliderValueChange: distanceSlider];
    // start timer to detect collision with radar line and blink
    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                            target:self
                                                          selector:@selector(detectCollisions:)
                                                          userInfo:nil
                                                           repeats:YES];
}

- (void)loadUsers {
    // empty the existing dots from radar
    [self removePreviousDots]; // this is useful if you want to remove existing dots at runtime
    
    // At this point use your own method to load nearby users from server via network request.
    // I'm using some dummy hardcoded users data.
    // Make sure in your returned data the **sorted** by **nearest to farthest**
    /*nearbyUsers = @[
     @{@"gender":@"female", @"lat":@48.859873, @"lng":@2.295083, @"distance":@173.1}, // *Nearest*
     @{@"gender":@"male",   @"lat":@48.859619, @"lng":@2.296101, @"distance":@180}, //
     @{@"gender":@"female", @"lat":@48.856492, @"lng":@2.298515, @"distance":@362.3}, // THE SORTING is
     @{@"gender":@"male",   @"lat":@48.859718, @"lng":@2.300544, @"distance":@468.6}, // Very IMPORTANT!
     @{@"gender":@"female", @"lat":@48.858376, @"lng":@2.287666, @"distance":@499.8}, //
     @{@"gender":@"male",   @"lat":@48.854643, @"lng":@2.289186, @"distance":@567.1}  // *Farthest*
     ];*/
    
//    NSMutableArray * outputArray = [NSMutableArray new];
    
    for (UserEntity *user in _user._userList)
    {
        [nearbyUsers addObject:@{@"gender": @"female", @"lat":@(user._latitude), @"lng":@(user._longitude), @"distance":@(user._distance)}];
    }
    
    [self renderUsersOnRadar:nearbyUsers];
}

#pragma mark - Spin the radar view continuously
-(void)spinRadar{
    /**** spin animation object ***/
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 5;
    spin.toValue = [NSNumber numberWithFloat:-M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO; // this is to keep on animating after application pause-resume
    spin.repeatCount = MAXFLOAT;
    radarLine.layer.anchorPoint = CGPointMake(-0.18, 0.5);
    
    [radarLine.layer addAnimation:spin forKey:@"spinRadarLine"];
    [radarView.layer addAnimation:spin forKey:@"spinRadarView"];
}

- (void)startHeadingEvent {
    if (nil == _locManager) {
        // Retain the object in a property.
        _locManager = [[CLLocationManager alloc] init];
        _locManager.delegate = self;
        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locManager.distanceFilter = kCLDistanceFilterNone;
        _locManager.headingFilter = kCLHeadingFilterNone;
    }
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([_locManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locManager requestWhenInUseAuthorization];
    }
    
    // Start heading updates.
    if ([CLLocationManager headingAvailable]) {
        [_locManager startUpdatingHeading];
    }
}

- (void)rotateArcsToHeading:(CGFloat)angle {
    // rotate the circle to heading degree
    arcsView.transform = CGAffineTransformMakeRotation(angle);
    // rotate all dots to opposite angle to keep the profile image straight up
    for (Dot *dot in dots) {
     dot.transform = CGAffineTransformMakeRotation(-angle);
     }
}

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);
    
    float degree = radiandsToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
    
    if (degree >= 0) {
        return -degree;
    } else {
        return -(360+degree);
    }
}

#pragma mark - Rotate/Trsnslate Dot

- (void)rotateDot:(Dot*)dot fromBearing:(CGFloat)fromDegrees toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathAddArc(path,nil, 140, 140, distance, degreesToRadians(fromDegrees), degreesToRadians(degrees), YES);
    
    CAKeyframeAnimation *theAnimation;
    
    // animation object for the key path
    theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    theAnimation.path=path;
    CGPathRelease(path);
    
    // set the animation properties
    theAnimation.duration=3;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.autoreverses = NO;
    theAnimation.fillMode = kCAFillModeForwards;
    theAnimation.cumulative = YES;
    
    
    
    CGPoint newPosition = CGPointMake(distance*cos(degreesToRadians(degrees))+(radarHolderWidth/ 2 - 2), distance*sin(degreesToRadians(degrees))+(radarHolderWidth/ 2 - 2));
    dot.layer.position = newPosition;    
    [dot.layer addAnimation:theAnimation forKey:@"rotateDot"];
    
}

- (void)translateDot:(Dot*)dot toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    
    [animation setFromValue:[NSValue valueWithCGPoint:[[dot.layer.presentationLayer valueForKey:@"position"] CGPointValue] ]];
    
    CGPoint newPosition = CGPointMake(distance*cos(degreesToRadians(degrees))+(radarHolderWidth/ 2 - 2), distance*sin(degreesToRadians(degrees))+(radarHolderWidth/ 2 - 2));
    [animation setToValue:[NSValue valueWithCGPoint: newPosition]];
    
    [animation setDuration:0.3f];
    animation.fillMode = kCAFillModeForwards;
    animation.autoreverses = NO;
    animation.repeatCount = 0;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [alphaAnimation setDuration:0.5f];
    alphaAnimation.fillMode = kCAFillModeForwards;
    alphaAnimation.autoreverses = NO;
    alphaAnimation.repeatCount = 0;
    alphaAnimation.removedOnCompletion = NO;
    alphaAnimation.cumulative = YES;
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    if (distance > (radarHolderWidth/ 2 - 8)) {
        [alphaAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
        
    }else{
        [alphaAnimation setToValue:[NSNumber numberWithFloat:1.0f]];
        
    }
    
    [dot.layer addAnimation:alphaAnimation forKey:@"alphaDot"];
    
    [dot.layer addAnimation:animation forKey:@"translateDot"];
    
//    NSLog(@"%ld", (long)dot.tag);
    
}


#pragma mark - Tap Event on Dot
- (void)onDotTapped:(UITapGestureRecognizer *)recognizer {
    
    UIView *circleView = recognizer.view;
    int tappedUser = 0;
    CGPoint point = [recognizer locationInView:circleView];
    // The for loop is to find out multiple dots in vicinity
    // you may define a NSMutableArray before the for loop and
    // get the group of dots together
    NSMutableArray *tappedUsers = [NSMutableArray array];
    
    for (Dot *d in dots) {
        if (d.zoomEnabled) {
            // remove selection from previously selected dot(s)
            d.zoomEnabled = NO;
            d.layer.borderColor = [UIColor clearColor].CGColor;
            [d setNeedsDisplay];
        }
        if([d.layer.presentationLayer hitTest:point] != nil){
            //[d.layer.presentationLayer setCornerRadius:5];
            // you can get the list of tapped user(s if more than one users are close enough)
            [tappedUsers addObject:d]; // use this variable outside of for loop to get list of users
            tappedUser = (int)d.tag;
            // Show white border for selected dot(s) and zoom out a little bit
            d.layer.borderColor = [UIColor whiteColor].CGColor;
            d.layer.borderWidth = 1;
            d.layer.cornerRadius = d.bounds.size.width/2;
            [d setNeedsDisplay];
            
            [self pulse:d];
            
            d.zoomEnabled = YES; // it'll keep a trace of selected dot(s)
            
        }
    }
    
    if (tappedUser > 0){
        UserEntity *user = [[UserEntity alloc] init];
        user = (UserEntity *)_user._userList[tappedUser - 101];
//        NSLog(@"%d" , user._idx);
        
        [self gotoMap: user];
    }
}

- (void) gotoMap : (UserEntity *) dotuser{
    
    MapViewController *mapVC = (MapViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    
    mapVC._from = FROM_RADAR;
    
    mapVC._selectedUser = dotuser;
    
    //self.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:mapVC animated:YES];
}


#pragma mark - Detect Collisions

- (void)detectCollisions:(NSTimer*)theTimer
{
    float radarLineRotation = radiandsToDegrees( [[radarLine.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"] floatValue] );
    
    if (radarLineRotation >= 0) {
        radarLineRotation -= 360;
    }
    
    
    for (int i = 0; i < [dots count]; i++) {
        Dot *dot = [dots objectAtIndex:i];
        float dotBearing = [dot.bearing floatValue] - currentDeviceBearing;
        
        if (dotBearing < -360) {
            dotBearing += 360;
        }
        
        // collision detection
        if( ABS(dotBearing - radarLineRotation) <=  20)
        {
            [self pulse:dot];            
        }
    }
}
-(void)pulse:(Dot*)dot{
    
    if([dot.layer.animationKeys containsObject:@"pulse"] || dot.zoomEnabled){ // view is already animating. so return
        return;
    }
    
    CABasicAnimation * pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.duration = 0.15;
    pulse.toValue = [NSNumber numberWithFloat:1.4];
    pulse.autoreverses = YES;
    dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    [dot.layer addAnimation:pulse forKey:@"pulse"];
}

#pragma mark - Slider
- (IBAction)sliderValueChange:(UISlider *)sender {
    float new_distance = [sender value];
    float distanceFilter = 0;
    
    for (int i = 0; i < [nearbyUsers count]; i++) {
        NSDictionary *user = nearbyUsers[i];
        
        float distance = [[user valueForKey:@"distance"] floatValue];
        float nextDistance = distance;
        //NSLog(@"distance %f <--->nextDistance %f ===>distanceFilter %f",distance, nextDistance, distanceFilter);
        
        if (i< [nearbyUsers count]-1) {
            NSDictionary *nextUser = nearbyUsers[i+1];
            nextDistance = [[nextUser valueForKey:@"distance"] floatValue];
        }
        
        if (distance <= new_distance && nextDistance >= new_distance) {
            if (nextDistance == new_distance) {
                distanceFilter = nextDistance;
            }else{
                distanceFilter = distance;
            }
            //NSLog(@"%f <---> %f ===> %f",distance, nextDistance, distanceFilter);
            break;
        }
    }
    //NSLog(@"distance filter %f", distanceFilter);
    [self filterNearByUsersByDistance: distanceFilter];
}

// for this function to work, sorting of users data by distance in ASC order (nearest to farthest) is a must
-(void) filterNearByUsersByDistance: (float)maxDistance{
    int i = 0;
    nearbyCount = 0;
    for (id d in dots) {
        i++;
        Dot *dot = (Dot *)d;
        
        //[dot.layer.presentationLayer setCornerRadius:5];
        float distance = MAX((radarHolderWidth/ 2 - 8) / 4,[dot.userDistance floatValue] * (radarHolderWidth/ 2 - 8) / maxDistance);//35
        [self translateDot:dot toBearing:[dot.bearing floatValue] atDistance: distance];
        
        dot.tag = i+100;
        
        if([dot.userDistance floatValue] <= maxDistance)
        {
            nearbyCount += 1;
        }
        
    }
    lblInfo.text = [NSString stringWithFormat:@"%d in %.2lfkm", nearbyCount, maxDistance];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    float heading = newHeading.magneticHeading; //in degrees
    float headingAngle = -(heading*M_PI/180); //assuming needle points to top of iphone. convert to radians
    currentDeviceBearing = heading;
    //    circle.transform = CGAffineTransformMakeRotation(headingAngle);
    [self rotateArcsToHeading:headingAngle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)decreaseDistanceTapped:(id)sender {
    distanceSlider.value += - (distanceSlider.maximumValue - distanceSlider.minimumValue) * 0.1;
    [self sliderValueChange: distanceSlider];
}

- (IBAction)increaseDistanceTapped:(id)sender {
    
    distanceSlider.value += (distanceSlider.maximumValue - distanceSlider.minimumValue) * 0.1;
    [self sliderValueChange: distanceSlider];

}

- (IBAction)registerAction:(id)sender {
    
    [self gotoRegister];
    
}

- (void) gotoRegister {
    
    RegisterViewController *regsiterVC = (RegisterViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    
    [self presentViewController:regsiterVC animated:YES completion:nil];
    
}

#pragma mark - tabarController delegate

- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    NSLog(@"user state : %d", [CommonUtils getUserState]);
    
    [CommonUtils loadUserInfo];
    
    UINavigationController *navController = (UINavigationController *) viewController;
    UIViewController * rootVC = navController.viewControllers[0];
    
    if (![rootVC isKindOfClass:[RadarViewController class]] && [CommonUtils getUserState] == STATE_UNREGISTERED) {
        
        NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:255/255.0 green:1/255.0 blue:1/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
        NSMutableAttributedString *attributedTitle;
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"]];
        NSString *alert_title = [APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, alert_title.length)];

        //[self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PASSWORD"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"]
                                                                       message:[APPDELEGATE.locationString objectForKey:@"ALERT_REGISTER"]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
        
        UIAlertAction * yesButton = [UIAlertAction actionWithTitle:[APPDELEGATE.locationString objectForKey:@"ALERT_CANCEL"] style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                           }];
        
        UIAlertAction * cancelButton = [UIAlertAction actionWithTitle:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self gotoRegister];
            
        }];
        
        [alert addAction:yesButton];
        [alert addAction:cancelButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return NO;
        
    } else {
        
        return YES;
    }
}

@end

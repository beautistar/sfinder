//
//  BaseTableViewController.h
//  SFinder
//
//  Created by AOC on 21/02/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseTableViewController : UITableViewController

- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;

@end

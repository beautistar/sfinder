//
//  MemberRegViewController.m
//  SFinder
//
//  Created by AOC on 15/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MemberRegViewController.h"
#import "CommonUtils.h"

@interface MemberRegViewController () {
    
    __weak IBOutlet UIButton *btnMakePayment;
    __weak IBOutlet UIButton *btnRegisterMember;
    __weak IBOutlet UIButton *btnSendCode;
    __weak IBOutlet UIButton *btnLogout;
    __weak IBOutlet UITextField *tfPhone;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfName;
    __weak IBOutlet UITextField *tfAuthCode;
    
    UserEntity *_user;
}

@end

@implementation MemberRegViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    btnMakePayment.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnMakePayment.layer.borderWidth = 1;

    btnRegisterMember.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnRegisterMember.layer.borderWidth = 1;
    
    btnSendCode.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnSendCode.layer.borderWidth = 1;
    
    btnLogout.layer.borderColor = [[UIColor whiteColor] CGColor];
    btnLogout.layer.borderWidth = 1;
    
    [self.tabBarController.tabBar setHidden:YES];
    
    [self initView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    tfPhone.text = _user._phone;
    tfEmail.text = _user._email;
    tfName.text = _user._name;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendAuthCodeAction:(id)sender {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETCODEFORUPDATE];
    
    NSDictionary * params = @{
                              EMAIL : tfEmail.text
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        [_user._userList removeAllObjects];
        
        NSLog(@"authcode respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"VALID_AUTHCODE"] duration:2] show];
            
        } else {          
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_EMAILADDRESS"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
    
}

- (IBAction)memberRegisterAction:(id)sender {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_REGISTERMEMBER];
    
    NSString *email = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *name = tfName.text;
    NSString *phone = tfPhone.text;
    
    NSDictionary * params = @{
                              EMAIL : email,
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude],
                              RES_NAME : name,
                              AUTHCODE : tfAuthCode.text,
                              RES_PHONE : phone,
                              RES_ADDRESS : _user._address
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"register respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_ID] intValue];
            _user._email = email;
            _user._name = name;
            _user._phone = phone;
            _user._userState = STATE_MEMBER;
            
            [CommonUtils saveUserInfo];
            
            [[JLToast makeText:[APPDELEGATE.locationString objectForKey:@"SUCCESS_MEMBERREGISTER"] duration:2] show];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else if (nResultCode == CODE_EXISTNAME) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else if (nResultCode == CODE_INVALIDEMAIL) {
            
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"EXIST_NONEEMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        } else  if(nResultCode == CODE_WRONGAUTHCODE) {
            [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INVALID_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
    
}


- (IBAction)makePaymentAction:(id)sender {
    
    
}
- (IBAction)termAction:(id)sender {
    
    
}

- (BOOL) isValid {
    
    if (_user._latitude == 0.0 || _user._longitude == 0 || _user._address.length == 0) {
        
        return NO;
    }
    
    if (tfPhone.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_PHONE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfEmail.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfName.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_USENAME"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (tfAuthCode.text.length == 0) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_AUTHCODE"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    if (![CommonUtils isValidEmail:tfEmail.text]) {
        
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }
    
    return YES;
}

- (BOOL) isValidForVerify {
    
    if (tfEmail.text.length == 0) {
     
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"INPUT_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
        
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {
        
       
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"VALID_EMAIL"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)logoutAction:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];    

    _user._idx = 0;
    _user._email = @"";
    _user._phone = @"";
    _user._name = @"";
    _user._aboutMe = @"";
    _user._userState = 0;
//    [CommonUtils saveUserInfo];
    _user._isLoggedIn = NO;
    [[UIApplication sharedApplication].keyWindow setRootViewController:(UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"IntroNav"]];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    return 210.f;
}

@end

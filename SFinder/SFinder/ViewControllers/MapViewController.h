//
//  MapViewController.h
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"
#import "BaseViewController.h"

@interface MapViewController : BaseViewController

@property (nonatomic) int _from;
@property (nonatomic, strong) UserEntity *_selectedUser;

@end

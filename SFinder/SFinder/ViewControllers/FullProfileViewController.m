//
//  FullProfileViewController.m
//  SFinder
//
//  Created by AOC on 24/01/17.
//  Copyright © 2017 Mobile. All rights reserved.
//

#import "FullProfileViewController.h"
#import "UITextView+Placeholder.h"

@interface FullProfileViewController() {
    
    __weak IBOutlet UITextView *tvAboutMe;
    __weak IBOutlet UILabel *lblPhone;
    __weak IBOutlet UIScrollView *scroll;
    __weak IBOutlet UIPageControl *pageControl;
    
    
    __weak IBOutlet UILabel *tfAge;
    __weak IBOutlet UILabel *tfDistance;
    __weak IBOutlet UILabel *tfheight;
    __weak IBOutlet UILabel *tfFigure;
    __weak IBOutlet UILabel *tfhairColor;
    
    int imageCount;
    CGFloat _scrollViewWidth;
    int _pageNumber;
    
    NSMutableArray *userFilters;
    
    NSArray *_user_filters;
    
    
}

@end

@implementation FullProfileViewController
@synthesize _fullUser;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    
    
}

- (void) initView {
    
    self.navigationItem.title = _fullUser._name;
    
    [self.navigationController setNavigationBarHidden:NO];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    
    tvAboutMe.layer.cornerRadius = 3;
    tvAboutMe.placeholder = @"About me...";
    lblPhone.text = _fullUser._phone;
    tvAboutMe.text = _fullUser._aboutMe;
    tfAge.text = [NSString stringWithFormat:@"%d", _fullUser._age];
    tfDistance.text = [NSString stringWithFormat:@"%.2f %@", _fullUser._distance, @"Km"];
    tfheight.text = [NSString stringWithFormat:@"%.1f %@", _fullUser._height, @"Cm"];
    tfFigure.text = _fullUser._figure;
    tfhairColor.text = _fullUser._hair_color;
    
    userFilters = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:_fullUser._anal], [NSNumber numberWithInt:_fullUser._bdsm], [NSNumber numberWithInt:_fullUser._bisexual], [NSNumber numberWithInt:_fullUser._blow_job], [NSNumber numberWithInt:_fullUser._delicate_sex], [NSNumber numberWithInt:_fullUser._deep_throat], [NSNumber numberWithInt:_fullUser._devot], [NSNumber numberWithInt:_fullUser._dirty_talks], [NSNumber numberWithInt:_fullUser._dominant], [NSNumber numberWithInt:_fullUser._escort_service], [NSNumber numberWithInt:_fullUser._hand_job], [NSNumber numberWithInt:_fullUser._classic], [NSNumber numberWithInt:_fullUser._kisses], [NSNumber numberWithInt:_fullUser._leather], [NSNumber numberWithInt:_fullUser._rolling_games], [NSNumber numberWithInt:_fullUser._masturbation], [NSNumber numberWithInt:_fullUser._spanish], [NSNumber numberWithInt:_fullUser._stripetease], [NSNumber numberWithInt:_fullUser._without_tabu], [NSNumber numberWithInt:_fullUser._transsexuel] ,nil];

    
    imageCount = (int)_fullUser._photoUrl.count;
    
    // Scroll View
    
    scroll.backgroundColor=[UIColor clearColor];
    scroll.delegate=self;
    scroll.pagingEnabled=YES;
    scroll.alwaysBounceVertical = NO;
    scroll.bounces = NO;
    
    [scroll setContentSize:CGSizeMake(self.view.frame.size.width*imageCount, self.view.frame.size.width)];
    
    // page control
    
    pageControl.backgroundColor=[UIColor clearColor];
    pageControl.numberOfPages=imageCount;
    [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    
    CGFloat x=0;
    
    if (imageCount == 0) {
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, self.view.frame.size.width)];
        
        [image setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"profile"]];
        
        [scroll addSubview:image];
        
    } else {
        
        for(int i=1;i<=imageCount;i++)
        {
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(x+0, 0, self.view.frame.size.width, self.view.frame.size.width)];
            //        for test
            //        [image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"image%d",i]]];
//                    [image setImage:[UIImage imageNamed:@"model"]];
            [image setImageWithURL:[NSURL URLWithString:_fullUser._photoUrl[i-1]] placeholderImage:[UIImage imageNamed:@"profile"]];
            
            [scroll addSubview:image];
            
            x+=self.view.frame.size.width;
        }
    }
    /////////
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    _scrollViewWidth = self.view.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    _pageNumber = floor((_scrollView.contentOffset.x - _scrollViewWidth/50) / _scrollViewWidth) +1;
    
    pageControl.currentPage=_pageNumber;
    
}

- (void)pageChanged {
    
    int pageNumber = (int)pageControl.currentPage;
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [scroll scrollRectToVisible:frame animated:YES];
}

- (void) autoScroll {
    
    pageControl.currentPage=_pageNumber++;
    
    if (_pageNumber >= imageCount) {
        
        _pageNumber = 0;
    }
    
    CGRect frame = scroll.frame;
    frame.origin.x = frame.size.width*_pageNumber;
    frame.origin.y=0;
    
    if (_pageNumber == 0) {
        
        [scroll scrollRectToVisible:frame animated:NO];
        
    } else {
        
        [scroll scrollRectToVisible:frame animated:YES];
    }
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) callAction {
    
    if (_fullUser._phone.length != 0) {
        
        
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",_fullUser._phone]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [alert show];
        }        
    }
}

#pragma mark - tableview delegate & datasource

//- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    
//    return 7;
//}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"filter : %d", [[userFilters objectAtIndex:indexPath.row] intValue]);
    
    if (indexPath.row == 0) {
        return self.view.frame.size.width;
    } else if ([[userFilters objectAtIndex:indexPath.row] intValue] == 0) {
        return 0;
    } else {
        return 60;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        
        [self callAction];
    }
}

@end

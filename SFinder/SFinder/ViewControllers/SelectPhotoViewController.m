//
//  SelectPhotoViewController.m
//  SFinder
//
//  Created by AOC on 01/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SelectPhotoViewController.h"
#import "CommonUtils.h"

@interface SelectPhotoViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource> {
    
    __weak IBOutlet UIImageView *imvPhoto;
    __weak IBOutlet UICollectionView *cvPhoto;
    __weak IBOutlet UILabel *lblIntro;
    
    UserEntity *_user;
}

@end


@implementation SelectPhotoViewController

@synthesize strPhotoPath;

@synthesize arrPhotoPath;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    arrPhotoPath = [NSMutableArray array];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (_user._userState == STATE_REGISTERED) {
        
        [lblIntro setHidden:NO];
    } else {
        [lblIntro setHidden:YES];
    }
}

- (IBAction)closeAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)cameraAction:(id)sender {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
    
    
}

- (IBAction)galleryAction:(id)sender {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}


#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        if (_user._userState == STATE_REGISTERED) {
            
            if (arrPhotoPath.count > 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Registered user can't attach multiple photoes." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                
                [alert show];
            }
            
        } else if (arrPhotoPath.count > 4) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Can't attach more than 5 photo" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        } else {
        
            dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
            
            dispatch_async(writeQueue, ^{
                
                NSString *savedFilePath = [CommonUtils saveToFile:chosenImage isProfile:YES];
                
                [arrPhotoPath addObject:savedFilePath];
                
    //            strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^ {
                    
                    [cvPhoto reloadData];
                    
                    // update ui (set profile image with saved Photo URL
                    [imvPhoto setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                });
            });
        }
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender {
    
    photoPath = strPhotoPath;
    
     _from = FROM_TAKEPHOTO;
    
    [self performSegueWithIdentifier:@"segueunwind2myprofile" sender:self];   

}

#pragma mark - collectionview Delegate & datasource
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    return 5;
    return arrPhotoPath.count;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((self.view.frame.size.width - 60) / 3.0, (self.view.frame.size.width - 60) / 3.0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"photo_cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
//    recipeImageView.image = [UIImage imageNamed:@"model"];
    [recipeImageView setImage:[UIImage imageWithContentsOfFile:arrPhotoPath[indexPath.row]]];
    
    return cell;
}

@end

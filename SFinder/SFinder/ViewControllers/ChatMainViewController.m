//
//  ChatMainViewController.m
//  SFinder
//
//  Created by AOC on 24/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ChatMainViewController.h"
#import "FullProfileViewController.h"
#import "ChattingViewController.h"
#import "ChatListCell.h"
#import "CommonUtils.h"

@interface ChatMainViewController() <PhotoTapDelegate, DCMessageDelegate> {
    
    UserEntity *_user;
}

@property (weak, nonatomic) IBOutlet UITableView *tblChatList;

@end

@implementation ChatMainViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    //[self.navigationController setNavigationBarHidden:YES];
    
    _tblChatList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO];
    self.tabBarController.tabBar.hidden = NO;
    
    APPDELEGATE.xmpp._messageDelegate = self;
    _user = APPDELEGATE.Me;
    bIsShownChatList = YES;
    
    [_tblChatList reloadData];
   
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    bIsShownChatList = NO;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

// ------------------------------------------------------------------------
#pragma mark - 
#pragma mark - tableview datasource & delegate
// ------------------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_user._roomList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChatListCell * cell = (ChatListCell *) [tableView dequeueReusableCellWithIdentifier:@"ChatListCell" forIndexPath:indexPath];
    
    cell.delegate = self;
    
    [cell setRoomModel:_user._roomList[indexPath.row]];
    
    return cell;
}

#pragma mark
#pragma mark - PhotoTapDelegate

-(void) photoTapped: (ChatListCell *) cell {
    
    NSIndexPath * indexPath = [_tblChatList indexPathForCell:cell];
    
    RoomEntity *tappedRoom = _user._roomList[indexPath.row];
    
    UserEntity *tappedParticipant = tappedRoom._participants[0];
    
    NSLog(@"indexpath : %ld", (long)indexPath.row);
    
    [self gotoFullDetail:tappedParticipant];
    
}

- (void) gotoFullDetail : (UserEntity *) _tappedUser {
    
//    PeopleProfileViewController * destVC = (PeopleProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"PeopleProfileViewController"];
    FullProfileViewController * destVC = (FullProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FullProfileViewController"];
    
    destVC._fullUser = _tappedUser;
    
    [self.navigationController pushViewController:destVC animated:YES];
}

- (void) gotoMap {
    
    UINavigationController *mapNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MapNav"];
    
    [self presentViewController:mapNav animated:YES completion:nil];
}

// ---------------------------------------------------------------
#pragma mark - DCMessage Delegate
// ---------------------------------------------------------------
//update the room information or add new room with new received packet
- (void) newPacketReceived:(XmppPacket *)_revPacket {
    
    [CommonUtils vibrate];
    
    //  check the user's room list & update
    // if user has not a room add it into usre's room list
    if(![self updatedExistRoom:_revPacket]) {
        
        NSArray *ids = [_revPacket._participantName componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx == _user._idx) {
                
                other = idx;
                break;
            }
        }
        
        // if user has not a room relation with new received packet, then add it into user's room lit
        // get participants info from a server
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETUSERINFOBYID, _revPacket._participantName];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                _entity._name = [_dictParticipant valueForKey:RES_NAME];
                _entity._latitude = [[_dictParticipant valueForKey:LATITUDE] floatValue];
                _entity._longitude = [[_dictParticipant valueForKey:LONGITUDE] floatValue];
                _entity._address = [_dictParticipant valueForKey:RES_ADDRESS];
                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
                _entity._age = [[_dictParticipant valueForKey:RES_AGE] intValue];
                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
                _entity._userState = [[_dictParticipant valueForKey:RES_ISMEMBER] intValue];
                _entity._phone = [_dictParticipant valueForKey:RES_PHONE];
                //_entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTO];
                
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                
                photoUrl = [_dictParticipant valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [_entity._photoUrl addObject:photo_];
                }

                _entity._distance = [[_dictParticipant valueForKey:RES_DISTANCE] floatValue];
                
                [_arrParticipants addObject:_entity];
                
                // make new room
                RoomEntity * _newRoom = [[RoomEntity alloc] initWithName:_revPacket._roomName participants:_arrParticipants];
                
                // update with new info
                _newRoom._recentContent = _revPacket._bodyString;
                
                _newRoom._recentDate = _revPacket._sentTime;
                _newRoom._recentCounter = 1;
                
                [_user addRoomList:_newRoom];
                
                [self reloadData];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) reloadData {
    
    [_tblChatList reloadData];
}

// update the room information with new received packet
- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket {
    
    for(RoomEntity * _entity in _user._roomList) {
        
        // update room info - unread message cnt and the lasted content
        // in case of inviting, participants, participants name and room display name
        if([_revPacket._roomName isEqualToString:_entity._name]) {
            
            // room content
            if(_revPacket._chatType == _TEXT) {
                _entity._recentContent = _revPacket._bodyString;
            } else {
                _entity._recentContent = @"Sent File";
            }
            
            // sending time and unread count
            _entity._recentDate = _revPacket._sentTime;
            _entity._recentCounter ++;
            
            // if room name is same with original, but participants are different, in case of inviting
            if(![_entity._participantsName isEqualToString:_revPacket._participantName]) {
                
                _entity._participantsName = _revPacket._participantName;
                
                [_entity updateParticipantWithPartName:_revPacket._participantName withBlock:^ {
                    
                    [self reloadData];
                }];
            }
            
            // in case of not change participants...
            [self reloadData];
            
            // update room database
            [[DBManager getSharedInstance] updateRoomWithName:_entity._name participant_name:_entity._participantsName recentMessage:_entity._recentContent recentTime:_entity._recentDate recetCounter:_entity._recentCounter];
            
            //            _user.notReadCount ++;
            
            // notify unread message
            //            [APPDELEGATE notifyReceiveNewMessage];
            
            return YES;
        }
    }
    return NO;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"SegueChatList2Chat"]) {
        
        // set chatting room
        
        NSIndexPath *selectedIndex = [self.tblChatList indexPathForSelectedRow];
        
        RoomEntity *_chatRoom = _user._roomList[selectedIndex.row];
        [APPDELEGATE.xmpp EnterChattingRoom:_chatRoom];
        
        ChattingViewController * chatVC = (ChattingViewController *)[segue destinationViewController];
        chatVC.chatRoom = _chatRoom;
        
    } /*else if([segue.identifier isEqualToString:@"SegueChatList2FriendsList"]) {
        
        SelectFriendViewController * selectFriendVC = (SelectFriendViewController *)[segue destinationViewController];
        selectFriendVC.fromWhere = FROM_CHATLIST;
    }*/
}



@end

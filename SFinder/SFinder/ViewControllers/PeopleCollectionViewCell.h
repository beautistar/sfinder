//
//  RadarCollectionViewCell.h
//  SFinder
//
//  Created by Steffen Gruenewald on 20/10/2016.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PeopleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPeople;
@property (weak, nonatomic) IBOutlet UILabel *lblNameAge;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceCity;


@end




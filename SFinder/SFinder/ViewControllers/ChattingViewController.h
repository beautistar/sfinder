//
//  ChattingViewController.h
//  SFinder
//
//  Created by AOC on 29/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomEntity.h"

@interface ChattingViewController : UIViewController {
    
    RoomEntity *chatRoom;
    
}

@property (nonatomic, strong) RoomEntity *chatRoom;

@end

//
//  PeoplesViewController.m
//  SFinder
//
//  Created by AOC on 07/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PeoplesViewController.h"
#import "MapViewController.h"
#import "UserListCell.h"
#import "CommonUtils.h"
#import "CarbonKit.h"

@interface PeoplesViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
    
    __weak IBOutlet UICollectionView *cvPeoples;
    
    UserEntity *_user;
    
    CarbonSwipeRefresh *refreshControl;
    int refreshCount;
    
}

@end

@implementation PeoplesViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
//    [self checkMe];
    
    _user = APPDELEGATE.Me;
    
    // refresh control
    refreshControl = [[CarbonSwipeRefresh alloc] initWithScrollView:cvPeoples];
    [refreshControl setMarginTop:0];
    [refreshControl setColors:@[[UIColor blackColor], [UIColor blackColor], [UIColor blackColor]]];
    [cvPeoples addSubview:refreshControl];
    
    [refreshControl addTarget:self action:@selector(userRefresh:) forControlEvents:UIControlEventValueChanged];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:NO];
    
    [self checkMe];
    
//    [self loadUserInfo];
}

- (void) checkMe {
    
    NSMutableArray *tmpUserList = [[NSMutableArray alloc] init];
    
    for (UserEntity *user in _user._userList) {
        
        if (_user._idx != user._idx) {
            
            [tmpUserList addObject:user];            
        }
    }
    
    [_user._userList removeAllObjects];
    
    for (UserEntity *user in tmpUserList) {
        
        [_user._userList addObject:user];
    }
    
    [cvPeoples reloadData];
}

#pragma mark - collection view datasource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    return 10;
    return _user._userList.count;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0f;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0f;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UserListCell *cell = (UserListCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"UserListCell" forIndexPath:indexPath];
    
//    UserEntity *cellUser = _user._userList[indexPath.row];
//    
//    if (_user._idx != cellUser._idx) {    
        [cell setItem:_user._userList[indexPath.row]];
//    }
    
    return cell;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat _cellWidth = self.view.frame.size.width / 2;
    
    return CGSizeMake(_cellWidth - 3, _cellWidth + 50);
    
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    [self gotoMap:_user._userList[indexPath.row]];
    
}

- (void) gotoMap : (UserEntity *) selectedUser {
    
    MapViewController *mapVC = (MapViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    
    mapVC._from = FROM_PEOPLE;
    mapVC._selectedUser = selectedUser;
    
    [self.navigationController pushViewController:mapVC animated:YES];

}

- (void) userRefresh : (id) sender {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self endRefreshing];
    });
}

- (void) endRefreshing {
    
    [refreshControl endRefreshing];
}

- (void) loadUserInfo {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETUSERINFO];
    
    NSDictionary * params = @{
                              LATITUDE : [NSNumber numberWithFloat:_user._latitude],
                              LONGITUDE : [NSNumber numberWithFloat:_user._longitude]
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"userinfo respond : %@", responseObject);
        
        if (_user._userList != nil)
            [_user._userList removeAllObjects];

        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        [self hideLoadingView];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSMutableArray *usersDict = [responseObject valueForKey:RES_USERINFO];
            
            for (NSDictionary *dict in usersDict) {
                
                UserEntity *other = [[UserEntity alloc] init];
                
                other._idx = [[dict valueForKey:RES_ID] intValue];
                other._name = [dict valueForKey:RES_NAME];
                other._latitude = [[dict valueForKey:LATITUDE] floatValue];
                other._longitude = [[dict valueForKey:LONGITUDE] floatValue];
                other._address = [dict valueForKey:RES_ADDRESS];
                other._sex = [[dict valueForKey:RES_SEX] intValue];
                other._age = [[dict valueForKey:RES_AGE] intValue];
                other._aboutMe = [dict valueForKey:RES_ABOUTME];
                other._userState = [[dict valueForKey:RES_ISMEMBER] intValue];
                other._phone = [dict valueForKey:RES_PHONE];
//                other._photoUrl = [dict valueForKey:RES_PHOTO];
                NSMutableArray *photoUrl = [[NSMutableArray alloc] init];
                photoUrl = [dict valueForKey:RES_PHOTO];
                
                for (NSString *photo_ in photoUrl) {
                    
                    [other._photoUrl addObject:photo_];
                }
                
                other._distance = [[dict valueForKey:RES_DISTANCE] floatValue];
                
                //for filter options
                other._age = [[dict valueForKey:RES_AGE] intValue];
                other._height = [[dict valueForKey:HEIGHT] floatValue];
                other._figure = [dict valueForKey:FIGURE];
                other._hair_color = [dict valueForKey:HAIR_COLOR];
                other._anal = [[dict valueForKey:ANAL] intValue];
                other._bdsm = [[dict valueForKey:BDSM] intValue];
                other._bisexual = [[dict valueForKey:BISEXUAL] intValue];
                other._blow_job = [[dict valueForKey:BLOW_JOB] intValue];
                other._delicate_sex = [[dict valueForKey:DELICATE_SEX] intValue];
                other._deep_throat = [[dict valueForKey:DEEP_THROAT] intValue];
                other._devot = [[dict valueForKey:DEVOT] intValue];
                other._dirty_talks = [[dict valueForKey:DIRTY_TALKS] intValue];
                other._dominant = [[dict valueForKey:DOMINIANT] intValue];
                other._escort_service = [[dict valueForKey:ESCORT_SERVICE] intValue];
                other._hand_job = [[dict valueForKey:HAND_JOB] intValue];
                other._classic = [[dict valueForKey:CLASSIC] intValue];
                other._kisses = [[dict valueForKey:KISSES] intValue];
                other._leather = [[dict valueForKey:LEATHER] intValue];
                other._rolling_games = [[dict valueForKey:ROLLING_GAMES] intValue];
                other._masturbation = [[dict valueForKey:MASTURBATION] intValue];
                other._spanish = [[dict valueForKey:SPANISH] intValue];
                other._stripetease = [[dict valueForKey:STRIPTEASE] intValue];
                other._without_tabu = [[dict valueForKey:WITHOUT_TABU] intValue];
                other._transsexuel = [[dict valueForKey:TRANSSEXUAL] intValue];
                
                if (_user._idx != other._idx)
                    [_user._userList addObject:other];
            }
        }
        
        [cvPeoples reloadData];
        
        [refreshControl endRefreshing];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:[APPDELEGATE.locationString objectForKey:@"ALERT_TITLE"] message:[APPDELEGATE.locationString objectForKey:@"CONN_ERROR"] positive:[APPDELEGATE.locationString objectForKey:@"ALERT_OK"] negative:nil];
    }];
}


@end
